
---------------------------------------------------------------------------------------------------------------------------
 Requirements
    ODE_CORE V2.2
    ODE_SUBVENTION V2.2
---------------------------------------------------------------------------------------------------------------------------
 Installation
    1) Installer le package
    2) Ajouter dans le composer.json -> autoload -> psr-4 
    "Astre\\": [
        "custom/include/Astre"
    ],
    3) Executer dans le terminal "composer dump-autoload --ignore-platform-reqs"
    4) Vérifier la configuration du connecteur sur la page Admin
---------------------------------------------------------------------------------------------------------------------------
	DEV 1
		Initialisation du package
        [OPS_engagement] Ajout du champ ligne_credit  
        [OPS_engagement] Le champ ligne_credit devient obligatoire
        [OPS_liquidation] Ajout des champs date_liquidation, date_mandat, date_paiement et numero_mandat
        [Administrateur] Création d'une page Admin
        [OPS_dispositif] Add Action Ajax pour la page admin
   
    DEV2 
        Nouvelle action par lot "Générer le fichier d'interface"
        [OPS_dossier] Création de la modale "Génération de fichier d'interface" ( tpl, js et css)
        [OPS_dossier] Création custom controlleur
        [Administrateur] Configuration des constantes du fichier d'interface
        [Astre] InterfacageLiquidation correspond au fichier de doc 

    DEV3
        Création tache planifiée mise à jour des dossiers depuis un fichier
        Création algo mise à jour informations astre
        [OPS_dossier] Création interface de tests 

    DEV4 
        Nouvelle action par lot "Télécharger les documents Astre"
        [OPS_dossier] Création de la modale "Télécharger les documents Astre" ( tpl, js et css)
        [OPS_dossier] Update custom controlleur

    DEV5
        Modification script mise à jour informations Astre pour prendre en compte la modification du statut du dossier
        [Administrateur] Statut en cas de liquidation, mandatement ou paiement

    DEV6   
        [OPS_liquidation] Suppression des champs date_liquidation, date_mandat, date_paiement et numero_mandat
        [OPS_liquidation] Ajout des champs liquidation_date, mandat_num, mandat_date, mandat_montant, paiement_num, paiement_date et paiement_montant
        [OPS_liquidation] Modification libellé champ montant_ttc

    DEV7   
        [Administrateur] Ajout configuration taille max des documents astre et types acceptés
        [OPS_dossier] Vérification de la taille et du type des pieces justificatives

    DEV8   
        [OPS_engagement] Ajout champ date_creation_astre 
        Update algo mise à jour dossier, ne pas décoder les montants ( avant 120,00 était égal à 12000 ) 
        Ajouter Astre dans le libellé du journal
        Ajouter dans le libellé du detail le nom du statut et l'étape de traitement
    DEV9
	
    DEV10
	[OPS_engagement] Mise à jour automatique des montant mandaté, payé et restant du
	[Administrateur] Correction erreur : Le bouton pour acceder à la page de configuration ne s'ajoute pas au groupe => Solution finale xD 
	[Astre] Interfacage liquidation : correction erreur longueur de ligne => 2 longueurs incorrectes
	[Tache planifiée] Correction du libellé qui ne s'affichait pas 
---------------------------------------------------------------------------------------------------------------------------
Evolutions
    Integrer la modification du helper OdeAjaxHelper.js au core
    Integrer la modification de OdeModal au core
    Integrer les services au core
    Integrer le controlleur OPS_dispositif au core
    Utiliser le composer-merge au lieu de copier à la main dans le composer.json
    Algo mise à jour : vérification unicité de l'engagement et de la liquidation ( Peut poser probleme si engagement en doublon )
    Ajouter un hook suppression liquidation pour mettre à jour automatiquement les informations de l'engagement
---------------------------------------------------------------------------------------------------------------------------



            
            
