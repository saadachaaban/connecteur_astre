<?php
$module_name = 'OPS_liquidation';
$viewdefs[$module_name] =
  array(
    'DetailView' =>
    array(
      'templateMeta' =>
      array(
        'form' =>
        array(
          'buttons' =>
          array(
            0 => 'EDIT',
            1 => 'DUPLICATE',
            2 => 'DELETE',
            3 => 'FIND_DUPLICATES',
          ),
        ),
        'maxColumns' => '2',
        'widths' =>
        array(
          0 =>
          array(
            'label' => '10',
            'field' => '30',
          ),
          1 =>
          array(
            'label' => '10',
            'field' => '30',
          ),
        ),
        'useTabs' => true,
        'tabDefs' =>
        array(
          'LBL_ONGLET_1' =>
          array(
            'newTab' => true,
            'panelDefault' => 'expanded',
          ),
        ),
      ),
      'panels' =>
      array(
        'LBL_ONGLET_1' =>
        array(

          0 =>
          array(
            0 =>
            array(
              'name' => 'ops_engagement_ops_liquidation_name',
            ),
            1 => ''
          ),

          1 =>
          array(
            0 => 'date_entered',
            1 => 'date_modified',
          ),

          2 =>
          array(
            0 => 'name',
            1 =>
            array(
              'name' => 'liquidation_date',
              'label' => 'LBL_LIQUIDATION_DATE',
            ),
          ),

          3 =>
          array(
            0 =>
            array(
              'name' => 'montant_ttc',
              'label' => 'LBL_MONTANT_TTC',
            ),
            1 => ''
          ),

          4 =>
          array(
            0 =>
            array(
              'name' => 'mandat_num',
              'label' => 'LBL_MANDAT_NUM',
            ),
            1 =>
            array(
              'name' => 'mandat_date',
              'label' => 'LBL_MANDAT_DATE',
            ),
          ),

          5 =>
          array(
            0 =>
            array(
              'name' => 'mandat_montant',
              'label' => 'LBL_MANDAT_MONTANT',
            ),
            1 => ''
          ),

          6 =>
          array(
            0 =>
            array(
              'name' => 'paiement_num',
              'label' => 'LBL_PAIEMENT_NUM',
            ),
            1 =>
            array(
              'name' => 'paiement_date',
              'label' => 'LBL_PAIEMENT_DATE',
            ),
          ),

          7 =>
          array(
            0 =>
            array(
              'name' => 'paiement_montant',
              'label' => 'LBL_PAIEMENT_MONTANT',
            ),
            1 => ''
          ),

          8 =>
          array(
            0 => 'description',
          ),
        ),
      ),
    ),
  );;
