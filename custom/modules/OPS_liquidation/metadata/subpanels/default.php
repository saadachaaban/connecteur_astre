<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$module_name = 'OPS_liquidation';
$subpanel_layout = array(
    'top_buttons' => array(
        array('widget_class' => 'SubPanelTopCreateButton'),
        array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => $module_name),
    ),

    'where' => '',

    'list_fields' => array(
        'name' => array(
            'vname' => 'LBL_NAME',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => '20%',
        ),
        'montant_ttc' => array(
            'vname' => 'LBL_MONTANT_TTC',
            'width' => '20%',
        ),
        'mandat_num' => array(
            'vname' => 'LBL_MANDAT_NUM',
            'width' => '20%',
        ),
        'mandat_montant' => array(
            'vname' => 'LBL_MANDAT_MONTANT',
            'width' => '20%',
        ),
        'paiement_num' => array(
            'vname' => 'LBL_PAIEMENT_NUM',
            'width' => '20%',
        ),
        'paiement_montant' => array(
            'vname' => 'LBL_PAIEMENT_MONTANT',
            'width' => '20%',
        ),
        'date_modified' => array(
            'vname' => 'LBL_DATE_MODIFIED',
            'width' => '10%',
        ),
        'edit_button' => array(
            'vname' => 'LBL_EDIT_BUTTON',
            'widget_class' => 'SubPanelEditButton',
            'module' => $module_name,
            'width' => '4%',
        ),
        'remove_button' => array(
            'vname' => 'LBL_REMOVE',
            'widget_class' => 'SubPanelRemoveButton',
            'module' => $module_name,
            'width' => '5%',
        ),
    ),
);
