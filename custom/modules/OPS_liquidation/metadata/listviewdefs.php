<?php
$module_name = 'OPS_liquidation';
$listViewDefs[$module_name] =
  array(
    'NAME' =>
    array(
      'width' => '32%',
      'label' => 'LBL_NAME',
      'default' => true,
      'link' => true,
    ),
    'OPS_ENGAGEMENT_OPS_LIQUIDATION_NAME' =>
    array(
      'type' => 'relate',
      'link' => true,
      'label' => 'LBL_OPS_ENGAGEMENT_OPS_LIQUIDATION_FROM_OPS_ENGAGEMENT_TITLE',
      'id' => 'OPS_ENGAGEMENT_OPS_LIQUIDATIONOPS_ENGAGEMENT_IDA',
      'width' => '10%',
      'default' => true,
    ),
    'LIQUIDATION_DATE' =>
    array(
      'type' => 'date',
      'label' => 'LBL_LIQUIDATION_DATE',
      'width' => '10%',
      'default' => true,
    ),
    'MONTANT_TTC' =>
    array(
      'type' => 'currency',
      'label' => 'LBL_MONTANT_TTC',
      'currency_format' => true,
      'width' => '10%',
      'default' => true,
    ),

    'MANDAT_NUM' =>
    array(
      'type' => 'varchar',
      'label' => 'LBL_MANDAT_NUM',
      'width' => '10%',
      'default' => true,
    ),
    'MANDAT_DATE' =>
    array(
      'type' => 'date',
      'label' => 'LBL_MANDAT_DATE',
      'width' => '10%',
      'default' => true,
    ),
    'MANDAT_MONTANT' =>
    array(
      'type' => 'currency',
      'label' => 'LBL_MANDAT_MONTANT',
      'width' => '10%',
      'default' => true,
    ),

    'PAIEMENT_NUM' =>
    array(
      'type' => 'varchar',
      'label' => 'LBL_PAIEMENT_NUM',
      'width' => '10%',
      'default' => true,
    ),
    'PAIEMENT_DATE' =>
    array(
      'type' => 'date',
      'label' => 'LBL_PAIEMENT_DATE',
      'width' => '10%',
      'default' => true,
    ),
    'PAIEMENT_MONTANT' =>
    array(
      'type' => 'currency',
      'label' => 'LBL_PAIEMENT_MONTANT',
      'width' => '10%',
      'default' => true,
    ),

    'ASSIGNED_USER_NAME' =>
    array(
      'width' => '9%',
      'label' => 'LBL_ASSIGNED_TO_NAME',
      'module' => 'Employees',
      'id' => 'ASSIGNED_USER_ID',
      'default' => false,
    ),
  );;
