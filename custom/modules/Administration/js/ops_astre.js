Object.getLength = function(obj) {
    var size = 0,key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

$(document).ready(function() {
    HtmlHelper.getListe().init("dispositif_list");
    ModulePopup.setClickOpenPopup("btn_add_dispositif","OPS_dispositif",true,"callBackAddDispositif");
    ModulePopup.addCallBackFunction("callBackAddDispositif", AstreAdmin.callBackAddDispositif);
    ModulePopup.initSelectRelation();
    ModulePopup.initDeleteRelation();
    ModulePopup.addCallBackFunction("callBackSelectRelation", AstreAdmin.callBackSelectRelation);
});

var HtmlHelper = (function($) {

    const Liste = (function() {

        var liste_id = "";
        var liste_dom = false;
        var liste_dom_hidden = false;
        var liste_hidden = "";
        var liste = [];

        return {

            init: function(id) {
                liste_id = id;
                liste_dom = ( $(`#${liste_id}`).length > 0 || $(`#${liste_id}_hidden`).length > 0 ) ? $(`#${liste_id}`) : false;
                liste_dom_hidden = ( $(`#${liste_id}_hidden`).length > 0 ) ? $(`#${liste_id}_hidden`) : false;
                liste_hidden = ( liste_dom_hidden !== false ) ? liste_dom_hidden.val() : "";
                liste = (  liste_hidden !== "" && liste_hidden.split('|').length > 0 ) ?  liste_hidden.split('|'): [];
                if ( liste.length > 0 ) Liste.setClickDeleteElement();
            },

            addLigne: function(ligne_id='', ligne_libelle='', ligne_detail = '') {
                const ligne = Liste.newLigne(ligne_id,ligne_libelle,ligne_detail);
                if ( liste_dom !== false || ligne === false ){
                    if ( $.inArray( ligne.element_id, liste) === -1 ) {
                        var ligne_html = `<li class="list-group-item" id="${ligne.id}" >`;
                        ligne_html    +=     `<span class="element-info" > <strong>${ligne.libelle}</strong> ${ligne.detail} </span>`;
                        ligne_html    +=     `<span role="delete-${liste_id}-element" class="badge badge-danger" title="Retirer de la liste" data-id="${ligne.element_id}" data-list-id="${liste_id}" > Retirer </span>`;
                        ligne_html    += `</li>`;
                        if ( liste.length === 0 ) liste_dom.empty();
                        liste_dom.append(ligne_html);
                        liste.push(ligne.element_id);
                        liste_dom_hidden.val( liste.join('|') );
                        Liste.setClickDeleteElement();
                    }
                }
            },

            newLigne: function( id = '', libelle = '' , detail = '' ) { 
                if ( id !== '' ) {
                    return {
                        element_id: id,
                        id: `${liste_id}_${id}`,
                        libelle: ( libelle !== '' ) ? libelle : '',
                        detail: ( detail !== '' ) ? ` ( ${detail} )` : '',
                    };
                }
                return false;
            },

            setClickDeleteElement: function(){
                $(`[role="delete-${liste_id}-element"]`).each(function() {
                    $(this).off().on('click', function () {
                        const empty_html = '<li class="list-group-item"><span class="element-info" > Aucun élément </span></li>';
                        var element_id = $(this).attr("data-id");
                        var list_id = $(this).attr("data-list-id");
                        HtmlHelper.getListe().init(list_id);
                        liste = liste.filter(function(item) {
                            return item !== element_id
                        });
                        if ( liste.length === 0 ){
                            liste_dom.empty().append(empty_html);
                            liste_dom_hidden.val("");
                        } else {
                            $(`#${list_id}_${element_id}`).remove(); 
                            liste_dom_hidden.val(liste.join('|'));
                        }
                    });
                });
            },
        }
        
    })();
    return {
        getListe: function() {
            return Object.create(Liste);
        }
    }
})(jQuery);

var ModulePopup = (function($) {

    return {

        setClickOpenPopup: function( bouton_id, module_name, multi = false, callback ){
            var type = ( multi === true ) ? "MultiSelect" : "";
            if ( $(`#${bouton_id}`).length > 0 && module_name !== "" ){
                window['callBackModulePopup'] = ModulePopup.callback;
                $(`#${bouton_id}`).off().on('click', function () {
                    var json_data = {
                        "call_back_function": 'callBackModulePopup',
                        "form_name": "DetailView",
                        "field_to_name_array": {
                            "id": "subpanel_id"
                        },
                        "passthru_data": {
                            'callback': callback
                        }
                    }
                    open_popup(module_name, 600, 400, "", true, true, json_data, type, true);
                });
            }
        },
        
        addCallBackFunction: function( callback_name, callback_function ){ 
            window[callback_name] = callback_function;
        },

        callback: function( data ){ 
            var data_formated = {};
            do {
                if ( data.selection_list !== undefined && Object.getLength(data.selection_list) !== 0 ){
                    data_formated = data.selection_list;
                    break;
                } 

                if ( data.name_to_value_array !== undefined && data.name_to_value_array.subpanel_id !== "" ){
                    data_formated = { 0: data.name_to_value_array.subpanel_id};
                }
                
            } while (0);
            window[data.passthru_data.callback](data_formated);
        },

        initSelectRelation: function(){ 
            $("[role=select-relation]").each(function() {
                $(this).click(function() {
                    var champ_id = $(this).attr("data-id");
                    var json_data = {
                        "call_back_function": "callBackSelectRelation",
                        "form_name":{
                            "champ_id": champ_id,
                        },
                        "field_to_name_array":{"id":"id","name":"name"}
                    };
                    open_popup("OPS_statut", 600, 400, "", true, true, json_data , "", true );
                });
            });
        },

        initDeleteRelation: function(){ 
            $("[role=delete-relation]").each(function() {
                $(this).click(function() {
                    var champ_id = $(this).attr("data-id");
                    $(`#${champ_id}`).val("");
                    $(`#display_${champ_id}`).val("");
                });
            });
        }

    }

})(jQuery);

var AstreAdmin = (function($) {

    return {

        /**
         * Fonction qui initialise le click sur le bouton "+" pour ajouter des dispositifs 
         * 
         * @return {void} 
        */
        callBackAddDispositif: function(data){
            if ( Object.getLength(data) > 0 ){
                var loading = function(){};
                var callBack = function(){
                    if ( typeof this.result === "string" ){
                        console.log(`Erreur callBack OPS_dispositif::getDispositifsName : `, this.result);
                    } else {
                        for (const [dispositif_id, dispositif_name] of Object.entries(this.result)) {
                            var Liste = HtmlHelper.getListe();
                            Liste.init("dispositif_list");
                            Liste.addLigne(dispositif_id,dispositif_name,'');
                        }
                    }
                };
                var query = OdeQueries.getAjaxActionQuery();
                query.setModule('OPS_dispositif');
                query.setAction("getDispositifsName");
                query.setPostData( { ids: data} );
                OdeAjax.getByAction( query, loading, callBack );
            }   
        },

        /**
        * Fonction qui initialise le click sur le bouton "+" pour ajouter des dispositifs 
        * 
        * @return {void} 
        */
        callBackSelectRelation: function(data){
            if ( Object.getLength(data) > 0 ){
                // On récupere les informations du champ en question 
                var champ_id = data.form_name.champ_id;
                // On récupere les valeurs de l'enregistrement séléctionné
                var value_id = data.name_to_value_array.id;
                var value_name = data.name_to_value_array.name;
                $(`#${champ_id}`).val(value_id);
                $(`#display_${champ_id}`).val(value_name);
            }   
        },

    }

})(jQuery);

