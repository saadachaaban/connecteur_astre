{literal}
    <link href="custom/modules/Administration/css/ops_astre.css" rel="stylesheet" type="text/css">
{/literal}
 
<div class="moduleTitle">
    <h2>Configuration du Connecteur Astre</h2> 
    <div class="clear"></div>
</div>

<BR>

<form id="ConfigureSettings" name="ConfigureSettings" enctype='multipart/form-data' method="POST" action="index.php?module=Administration&action=ops_astre&process=true">

    <table width="100%" cellpadding="0" cellspacing="1" border="0" class="actionsContainer">
        <tr>
            <td>
                <input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" type="submit" name="save" onclick="return verify_data('ConfigureSettings');" value="{$APP.LBL_SAVE_BUTTON_LABEL}"> &nbsp;
                <input title="{$MOD.LBL_CANCEL_BUTTON_TITLE}" onclick="document.location.href='index.php?module=Administration&action=index'" class="button" type="button" name="cancel" value="  {$APP.LBL_CANCEL_BUTTON_LABEL}  "> 
            </td>
        </tr>
    </table>

    <br /> 

	<div class="admin-container" >

        <div class="row" style="background: white;margin: 0px;margin-bottom: 15px; padding-top: 10px">
            <h4> Paramètres globaux </h4>
            <div style="display: block;border-bottom: 2px solid #eee;margin-top: 10px;"></div>
        </div>

		<br>

        <div class="admin-params-container" >

            <div class="row">
                <div class="col-md-2">
                    <label>Dispositifs :<span id="btn_add_dispositif"class="badge badge-success add-element" title="Ajouter un dispositif à la liste"> + </span> </label>
                </div>
                <div class="col-md-4" >
                    <ul id="dispositif_list" class="list-group">
                        {if count($dispositifs) == 0}
                            <li class="list-group-item">
                                <span class="element-info" > Aucun élément </span>
                            </li>
                        {else}
                            {foreach from=$dispositifs key=dispositif_id item=dispositif_name}
                                <li class="list-group-item" id="dispositif_list_{$dispositif_id}" >
                                    <span class="element-info" > <strong>{$dispositif_name}</strong> </span>
                                    <span role="delete-dispositif_list-element" class="badge badge-danger" title="Retirer l'element de la liste" data-id="{$dispositif_id}" data-list-id="dispositif_list"> Retirer </span> 
                                </li>
                            {/foreach}
                        {/if}
                    </ul>
                    <input type="hidden" id="dispositif_list_hidden" name="dispositif_list_hidden" value="{$dispositif_list_hidden_value}" />
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-4"></div>
            </div>

        </div>

	    <br>

        <div class="row" style="background: white;margin: 0px;margin-bottom: 15px; padding-top: 10px">
            <h4> Constantes fichier d'interface </h4>
            <div style="display: block;border-bottom: 2px solid #eee;margin-top: 10px;"></div>
        </div>

		<br>

        <div class="admin-params-container" >

            <div class="row">
                <div class="col-md-2">    
                    <label>Organisme :</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="organisme" id="organisme" value="{$organisme}" />
                </div>
                <div class="col-md-2">
                    <label>Code budgétaire :</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="code_budget" id="code_budget" value="{$code_budget}" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">    
                    <label>Niveau de trait (0,1,2,3):</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="niv_trait" id="niv_trait" value="{$niv_trait}" />
                </div>
                <div class="col-md-2">
                    <label>Module de génération :</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="code_module" id="code_module" value="{$code_module}" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">    
                    <label>Type de mouvement (C,A) :</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="type_mouvement" id="type_mouvement" value="{$type_mouvement}" />
                </div>
                <div class="col-md-2">
                    <label>Type liquidation (I,C,M,R) :</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="type_liquidation" id="type_liquidation" value="{$type_liquidation}" />
                </div>
            </div>
   
            <div class="row">
                <div class="col-md-2">    
                    <label>Type d’édition demandée :</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="type_edit" id="type_edit" value="{$type_edit}" />
                </div>
                <div class="col-md-2">
                    <label>Autorisation maj gf :</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="ind_maj" id="ind_maj" value="{$ind_maj}" />
                </div>
            </div>
            
        </div>

	    <br>

        <div class="row" style="background: white;margin: 0px;margin-bottom: 15px; padding-top: 10px">
            <h4> Mise à jour des informations Astre </h4>
            <div style="display: block;border-bottom: 2px solid #eee;margin-top: 10px;"></div>
        </div>

		<br>

        <div class="admin-params-container" >

            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Statut liquidation :</label>
                        </div>
                        <div class="col-md-8">
                            <div class="col-md-7" style="padding: 0px;">
                                <input type="text" id="display_statut_liquidation" role="display-relation" data-id="statut_liquidation" disabled style="width: 100%; height: 47px; border: 1px solid #b8daef;background: #c3e7fd;" value="{$display_statut_liquidation}">
                                <input type="hidden" id="statut_liquidation" name="statut_liquidation" value="{$statut_liquidation}">
                            </div>
                            <div class="col-md-4" style="text-align: center; padding: 0px;">
                                <span class="id-ff multiple">
                                    <button role="select-relation" type="button" id="btn_statut_initial" data-id="statut_liquidation" title="Sélectionner" class="button firstChild" value="Sélectionner">
                                        <img src="themes/SuiteP/images/id-ff-select.png?v=9PUULME3DHwNvxEgZrpwig">
                                    </button>
                                    <button role="delete-relation" type="button" id="btn_clr_statut_initial" data-id="statut_liquidation" title="Clear Selection" class="button lastChild" value="Supprimer la selection">
                                        <img src="themes/SuiteP/images/id-ff-clear.png?v=9PUULME3DHwNvxEgZrpwig">
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>Url fichier de mise à jour :</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="path_file_update" id="path_file_update" value="{$path_file_update}" />
                </div>
            </div>

            <div class="row">

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Statut mandatement:</label>
                        </div>
                        <div class="col-md-8">
                            <div class="col-md-7" style="padding: 0px;">
                                <input type="text" id="display_statut_mandatement" role="display-relation" data-id="statut_mandatement" disabled style="width: 100%; height: 47px; border: 1px solid #b8daef;background: #c3e7fd;" value="{$display_statut_mandatement}">
                                <input type="hidden" id="statut_mandatement" name="statut_mandatement" value="{$statut_mandatement}">
                            </div>
                            <div class="col-md-4" style="text-align: center; padding: 0px;">
                                <span class="id-ff multiple">
                                    <button role="select-relation" type="button" id="btn_statut_initial" data-id="statut_mandatement" title="Sélectionner" class="button firstChild" value="Sélectionner">
                                        <img src="themes/SuiteP/images/id-ff-select.png?v=9PUULME3DHwNvxEgZrpwig">
                                    </button>
                                    <button role="delete-relation" type="button" id="btn_clr_statut_initial" data-id="statut_mandatement" title="Clear Selection" class="button lastChild" value="Supprimer la selection">
                                        <img src="themes/SuiteP/images/id-ff-clear.png?v=9PUULME3DHwNvxEgZrpwig">
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Statut paiement:</label>
                        </div>
                        <div class="col-md-8">
                            <div class="col-md-7" style="padding: 0px;">
                                <input type="text" id="display_statut_paiement" role="display-relation" data-id="statut_paiement" disabled style="width: 100%; height: 47px; border: 1px solid #b8daef;background: #c3e7fd;" value="{$display_statut_paiement}">
                                <input type="hidden" id="statut_paiement" name="statut_paiement" value="{$statut_paiement}">
                            </div>
                            <div class="col-md-4" style="text-align: center; padding: 0px;">
                                <span class="id-ff multiple">
                                    <button role="select-relation" type="button" id="btn_statut_initial" data-id="statut_paiement" title="Sélectionner" class="button firstChild" value="Sélectionner">
                                        <img src="themes/SuiteP/images/id-ff-select.png?v=9PUULME3DHwNvxEgZrpwig">
                                    </button>
                                    <button role="delete-relation" type="button" id="btn_clr_statut_initial" data-id="statut_paiement" title="Clear Selection" class="button lastChild" value="Supprimer la selection">
                                        <img src="themes/SuiteP/images/id-ff-clear.png?v=9PUULME3DHwNvxEgZrpwig">
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

	    <br>

        <div class="row" style="background: white;margin: 0px;margin-bottom: 15px; padding-top: 10px">
            <h4> Envoie de documents sur Astre </h4>
            <div style="display: block;border-bottom: 2px solid #eee;margin-top: 10px;"></div>
        </div>

		<br>

        <div class="admin-params-container" >

            <div class="row">
                <div class="col-md-2">    
                    <label>Taille max :</label>
                </div>
                <div class="col-md-4">
                    <input type="number" name="document_max_size" id="document_max_size" value="{$document_max_size}" />
                     <span style="font-style: italic;">kilooctet (ko)</span>
                </div>
                <div class="col-md-2">
                    <label>Types autorisés :</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="document_types" id="document_types" value="{$document_types}" />
                </div>
            </div>

        </div>

    <div style="padding-top: 2px;">
        <input title="{$APP.LBL_SAVE_BUTTON_TITLE}" class="button primary" id="submit" type="submit" name="save" value="{$APP.LBL_SAVE_BUTTON_LABEL}" /> &nbsp;
        <input title="{$MOD.LBL_CANCEL_BUTTON_TITLE}" onclick="document.location.href='index.php?module=Administration&action=index'" class="button" type="button" name="cancel" value="  {$APP.LBL_CANCEL_BUTTON_LABEL}  " />
    </div>
    {$JAVASCRIPT}
</form>

{literal}
    <script type="text/javascript" charset="UTF-8" src="custom/include/Ode/Helper/OdeAjaxHelper.js"></script>
    <script type="text/javascript" charset="UTF-8" src="custom/include/Ode/Helper/OdeQueriesHelper.js"></script>
    <script type="text/javascript" charset="UTF-8" src="custom/include/Ode/Helper/OdeLoadingHelper.js"></script>
    <script type="text/javascript" charset="UTF-8" src="custom/modules/Administration/js/ops_astre.js"></script>
{/literal}
