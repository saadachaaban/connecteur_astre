<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

global $current_user, $sugar_config, $app_list_strings;

if (!is_admin($current_user)) sugar_die("Unauthorized access to administration.");

require_once('modules/Configurator/Configurator.php');

use BeanFactory;

$configurator = new Configurator();
$sugar_smarty    = new Sugar_Smarty();
$errors        = array();

// Si process == true => On sauvegarde les données 
if (isset($_REQUEST['process']) && $_REQUEST['process'] == 'true') {

    // Paramètres globaux
    if (isset($_REQUEST['dispositif_list_hidden'])) {
        $configurator->config['opensub']['astre']['dispositifs'] = $_REQUEST['dispositif_list_hidden'];
    }
    // Constantes fichier d'interface
    if (isset($_REQUEST['organisme'])) {
        $configurator->config['opensub']['astre']['organisme'] = $_REQUEST['organisme'];
    }
    if (isset($_REQUEST['code_budget'])) {
        $configurator->config['opensub']['astre']['code_budget'] = $_REQUEST['code_budget'];
    }
    if (isset($_REQUEST['niv_trait'])) {
        $configurator->config['opensub']['astre']['niv_trait'] = $_REQUEST['niv_trait'];
    }
    if (isset($_REQUEST['code_module'])) {
        $configurator->config['opensub']['astre']['code_module'] = $_REQUEST['code_module'];
    }
    if (isset($_REQUEST['type_mouvement'])) {
        $configurator->config['opensub']['astre']['type_mouvement'] = $_REQUEST['type_mouvement'];
    }
    if (isset($_REQUEST['type_liquidation'])) {
        $configurator->config['opensub']['astre']['type_liquidation'] = $_REQUEST['type_liquidation'];
    }
    if (isset($_REQUEST['type_edit'])) {
        $configurator->config['opensub']['astre']['type_edit'] = $_REQUEST['type_edit'];
    }
    if (isset($_REQUEST['ind_maj'])) {
        $configurator->config['opensub']['astre']['ind_maj'] = $_REQUEST['ind_maj'];
    }

    // Mise à jour des informations Astre
    if (isset($_REQUEST['path_file_update'])) {
        $configurator->config['opensub']['astre']['path_file_update'] = $_REQUEST['path_file_update'];
    }
    if (isset($_REQUEST['statut_liquidation'])) {
        $configurator->config['opensub']['astre']['statut_liquidation'] = $_REQUEST['statut_liquidation'];
    }
    if (isset($_REQUEST['statut_mandatement'])) {
        $configurator->config['opensub']['astre']['statut_mandatement'] = $_REQUEST['statut_mandatement'];
    }
    if (isset($_REQUEST['statut_paiement'])) {
        $configurator->config['opensub']['astre']['statut_paiement'] = $_REQUEST['statut_paiement'];
    }

    // Envoie de documents sur Astre
    if (isset($_REQUEST['document_max_size'])) {
        $configurator->config['opensub']['astre']['document_max_size'] = $_REQUEST['document_max_size'];
    }
    if (isset($_REQUEST['document_types'])) {
        $configurator->config['opensub']['astre']['document_types'] = $_REQUEST['document_types'];
    }

    $configurator->handleOverride();
    header('Location: index.php?module=Administration&action=index');
}

// Initialisation de la page admin

// Paramètres globaux
$dispositif_list_hidden_value = (!empty($configurator->config['opensub']['astre']['dispositifs'])) ? $configurator->config['opensub']['astre']['dispositifs'] : "";
$dispositifs = getDispositifsInfos($dispositif_list_hidden_value);
$sugar_smarty->assign("dispositif_list_hidden_value", $dispositif_list_hidden_value);
$sugar_smarty->assign("dispositifs", $dispositifs);


// Constantes fichier d'interface
$organisme = (isset($configurator->config['opensub']['astre']['organisme'])) ? $configurator->config['opensub']['astre']['organisme'] : "";
$code_budget = (isset($configurator->config['opensub']['astre']['code_budget'])) ? $configurator->config['opensub']['astre']['code_budget'] : "";
$niv_trait = (isset($configurator->config['opensub']['astre']['niv_trait'])) ? $configurator->config['opensub']['astre']['niv_trait'] : "";
$code_module = (isset($configurator->config['opensub']['astre']['code_module'])) ? $configurator->config['opensub']['astre']['code_module'] : "";
$type_mouvement = (isset($configurator->config['opensub']['astre']['type_mouvement'])) ? $configurator->config['opensub']['astre']['type_mouvement'] : "";
$type_liquidation = (isset($configurator->config['opensub']['astre']['type_liquidation'])) ? $configurator->config['opensub']['astre']['type_liquidation'] : "";
$type_edit = (isset($configurator->config['opensub']['astre']['type_edit'])) ? $configurator->config['opensub']['astre']['type_edit'] : "";
$ind_maj = (isset($configurator->config['opensub']['astre']['ind_maj'])) ? $configurator->config['opensub']['astre']['ind_maj'] : "";
$sugar_smarty->assign("organisme", $organisme);
$sugar_smarty->assign("code_budget", $code_budget);
$sugar_smarty->assign("niv_trait", $niv_trait);
$sugar_smarty->assign("code_module", $code_module);
$sugar_smarty->assign("type_mouvement", $type_mouvement);
$sugar_smarty->assign("type_liquidation", $type_liquidation);
$sugar_smarty->assign("type_edit", $type_edit);
$sugar_smarty->assign("ind_maj", $ind_maj);

// Mise à jour des informations Astre
$path_file_update = (!empty($configurator->config['opensub']['astre']['path_file_update'])) ? $configurator->config['opensub']['astre']['path_file_update'] : "";
$statut_liquidation = (!empty($configurator->config['opensub']['astre']['statut_liquidation'])) ? $configurator->config['opensub']['astre']['statut_liquidation'] : "";
$statut_mandatement = (!empty($configurator->config['opensub']['astre']['statut_mandatement'])) ? $configurator->config['opensub']['astre']['statut_mandatement'] : "";
$statut_paiement = (!empty($configurator->config['opensub']['astre']['statut_paiement'])) ? $configurator->config['opensub']['astre']['statut_paiement'] : "";
$sugar_smarty->assign("path_file_update", $path_file_update);
$sugar_smarty->assign("statut_liquidation", $statut_liquidation);
$sugar_smarty->assign("statut_mandatement", $statut_mandatement);
$sugar_smarty->assign("statut_paiement", $statut_paiement);
$sugar_smarty->assign("display_statut_liquidation", getStatutName($statut_liquidation));
$sugar_smarty->assign("display_statut_mandatement", getStatutName($statut_mandatement));
$sugar_smarty->assign("display_statut_paiement", getStatutName($statut_paiement));

// Envoie de documents sur Astre
$document_max_size = (isset($configurator->config['opensub']['astre']['document_max_size'])) ? $configurator->config['opensub']['astre']['document_max_size'] : 0;
$document_types = (isset($configurator->config['opensub']['astre']['document_types'])) ? $configurator->config['opensub']['astre']['document_types'] : "";
$sugar_smarty->assign("document_max_size", $document_max_size);
$sugar_smarty->assign("document_types", $document_types);

$sugar_smarty->clear_cache('custom/modules/Administration/ops_astre.tpl');
$sugar_smarty->display('custom/modules/Administration/ops_astre.tpl');

function getDispositifsInfos($dispositifs_value)
{
    $dispositifs = [];
    if (!empty($dispositifs_value)) {
        $dispositifs_ids = explode("|", $dispositifs_value);
        if (is_array($dispositifs_ids) && count($dispositifs_ids) > 0) {
            foreach ($dispositifs_ids as $dispositif_id) {
                $obj_dispositif = BeanFactory::getBean("OPS_dispositif", $dispositif_id);
                if (!empty($obj_dispositif->name)) {
                    $dispositifs[$dispositif_id] = $obj_dispositif->name;
                }
            }
        }
    }
    return $dispositifs;
}


function getStatutName($statut_id = '')
{
    $obj_statut = BeanFactory::getBean("OPS_statut", $statut_id);
    return (!empty($obj_statut->name)) ? $obj_statut->name : '';
}
