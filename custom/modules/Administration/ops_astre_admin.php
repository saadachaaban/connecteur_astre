<?php


// Ajouter le connecteur à la page de configuration selon le package installé
$page_admin_astre_added = false;
$page_admin_bouton = [
    'Administration',
    'LBL_ASTRE',
    'LBL_ASTRE_PARAMS',
    './index.php?module=Administration&action=ops_astre',
    'system-settings'
];

//Choix n°1 l'ajouter au groupe "Open Subvention - Extention Île de la Réunion"
foreach ($admin_group_header as $index => $groupe) {
    if (is_array($groupe) && !empty($groupe[0]) && $groupe[0] === "LBL_CD974_ADMIN" && is_array($groupe[3]["CD974_admin"])) {
        $admin_group_header[$index][3]["CD974_admin"]["02_connecteur_astre"] = $page_admin_bouton;
        $page_admin_astre_added = true;
    }
}

// Choix n°2 dans le cas ou L'extension n'est pas installée on l'ajoute au groupe "Open Socle - Interopérabilité" 
if ($page_admin_astre_added === false) {
    foreach ($admin_group_header as $index => $groupe) {
        if (is_array($groupe) && !empty($groupe[0]) && $groupe[0] === "LBL_ODE_COMMUNICATION" && is_array($groupe[3]["ODE_admin"])) {
            $admin_group_header[$index][3]["ODE_admin"]["04_connecteur_astre"] = $page_admin_bouton;
            $page_admin_astre_added = true;
        }
    }
}

// Choix n°3 dans le cas ou il n'arrive pas à l'ajouter ni au core ni à l'extension => on crée un nouveau groupe
if ($page_admin_astre_added === false) {
    $admin_astre_defs = array();
    $admin_astre_defs['ODE_connecteur_astre_admin']['01_connecteur_astre'] = $page_admin_bouton;
    $admin_group_header[] = array('LBL_CONNECTEUR_ASTRE_ADMIN', '', false, $admin_astre_defs, '');
}
