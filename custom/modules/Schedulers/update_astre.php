<?php

use Astre\Controller\FileImportController;

$job_strings[] = 'update_astre';

function update_astre()
{
    $GLOBALS['log']->fatal(" [Tache planifiée][Start] ------ Mise à jour des informations Astre ------ ");
    $FileImportCtrl = new FileImportController();
    $FileImportCtrl->updateInformationAstre();
    $GLOBALS['log']->fatal(" [Tache planifiée][End] ------  Mise à jour des informations Astre ------ ");
    return true;
}
