<?php
$module_name = 'OPS_engagement';
$listViewDefs[$module_name] =
  array(
    'NAME' =>
    array(
      'width' => '32%',
      'label' => 'LBL_NAME',
      'default' => true,
      'link' => true,
    ),
    'LIGNE_CREDIT' =>
    array(
      'type' => 'varchar',
      'label' => 'LBL_LIGNE_CREDIT',
      'width' => '10%',
      'default' => true,
    ),
    'DATE_CREATION_ASTRE' =>
    array(
      'type' => 'date',
      'label' => 'LBL_DATE_CREATION_ASTRE',
      'width' => '10%',
      'default' => true,
    ),
    'OPS_ENGAGEMENT_OPS_DOSSIER_NAME' =>
    array(
      'type' => 'relate',
      'link' => true,
      'label' => 'LBL_OPS_DOSSIER_OPS_ENGAGEMENT_FROM_OPS_DOSSIER_TITLE',
      'id' => 'OPS_DOSSIER_ID',
      'width' => '10%',
      'default' => true,
    ),
    'MONTANT_TTC' =>
    array(
      'type' => 'currency',
      'label' => 'LBL_MONTANT_TTC',
      'currency_format' => true,
      'width' => '10%',
      'default' => true,
    ),
    'MONTANT_LIQUIDE' =>
    array(
      'type' => 'currency',
      'label' => 'LBL_MONTANT_LIQUIDE',
      'currency_format' => true,
      'width' => '10%',
      'default' => true,
    ),
    'MONTANT_PAYE' =>
    array(
      'type' => 'currency',
      'label' => 'LBL_MONTANT_PAYE',
      'currency_format' => true,
      'width' => '10%',
      'default' => true,
    ),
    'TYPE_ENGAGEMENT' =>
    array(
      'type' => 'enum',
      'studio' => 'visible',
      'label' => 'LBL_TYPE_ENGAGEMENT',
      'width' => '10%',
      'default' => true,
    ),
  );;
