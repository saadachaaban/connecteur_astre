<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$module_name = 'OPS_engagement';
$subpanel_layout = array(
    'top_buttons' => array(
        array('widget_class' => 'SubPanelTopCreateButton'),
        array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => $module_name),
    ),

    'where' => '',

    'list_fields' => array(
        'name' => array(
            'vname' => 'LBL_NAME',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => '30%',
        ),
        'montant_ttc' => array(
            'vname' => 'LBL_MONTANT_TTC',
            'width' => '20%',
        ),
        'montant_liquide' => array(
            'vname' => 'LBL_MONTANT_LIQUIDE',
            'width' => '20%',
        ),
        'montant_mandate' => array(
            'vname' => 'LBL_MONTANT_MANDATE',
            'width' => '20%',
        ),
        'montant_paye' => array(
            'vname' => 'LBL_MONTANT_PAYE',
            'width' => '20%',
        ),
        'montant_restant_du' => array(
            'vname' => 'LBL_MONTANT_RESTANT_DU',
            'width' => '20%',
        ),
        'date_modified' => array(
            'vname' => 'LBL_DATE_MODIFIED',
            'width' => '45%',
        ),
        'edit_button' => array(
            'vname' => 'LBL_EDIT_BUTTON',
            'widget_class' => 'SubPanelEditButton',
            'module' => $module_name,
            'width' => '4%',
        ),
        'remove_button' => array(
            'vname' => 'LBL_REMOVE',
            'widget_class' => 'SubPanelRemoveButton',
            'module' => $module_name,
            'width' => '5%',
        ),
    ),
);
