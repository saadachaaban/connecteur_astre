<?php
$module_name = 'OPS_engagement';
$viewdefs[$module_name] =
  array(
    'DetailView' =>
    array(
      'templateMeta' =>
      array(
        'form' =>
        array(
          'buttons' =>
          array(
            0 => 'EDIT',
            1 => 'DUPLICATE',
            2 => 'DELETE',
            3 => 'FIND_DUPLICATES',
          ),
        ),
        'maxColumns' => '2',
        'widths' =>
        array(
          0 =>
          array(
            'label' => '10',
            'field' => '30',
          ),
          1 =>
          array(
            'label' => '10',
            'field' => '30',
          ),
        ),
        'useTabs' => true,
        'tabDefs' =>
        array(
          'LBL_ONGLET_1' =>
          array(
            'newTab' => true,
            'panelDefault' => 'expanded',
          ),
        ),
      ),
      'panels' =>
      array(
        'LBL_ONGLET_1' =>
        array(
          0 =>
          array(
            0 => 'name',
            1 =>
            array(
              'name' => 'ligne_credit',
              'label' => 'LBL_LIGNE_CREDIT',
            ),
          ),
          1 =>
          array(

            0 => array(
              'name' => 'ops_engagement_ops_dossier_name',
            ),
            1 =>
            array(
              'name' => 'date_creation_astre',
              'label' => 'LBL_DATE_CREATION_ASTRE',
            ),
          ),
          2 =>
          array(
            0 =>
            array(
              'name' => 'montant_ttc',
              'label' => 'LBL_MONTANT_TTC',
            ),
            1 =>
            array(
              'name' => 'montant_liquide',
              'label' => 'LBL_MONTANT_LIQUIDE',
            ),
          ),
          3 =>
          array(
            0 =>
            array(
              'name' => 'montant_mandate',
              'label' => 'LBL_MONTANT_MANDATE',
            ),
            1 =>
            array(
              'name' => 'montant_paye',
              'label' => 'LBL_MONTANT_PAYE',
            ),
          ),
          4 =>
          array(
            0 =>
            array(
              'name' => 'montant_restant_du',
              'label' => 'LBL_MONTANT_RESTANT_DU',
            ),
            1 => array(
              'name' => 'type_engagement',
              'studio' => 'visible',
              'label' => 'LBL_TYPE_ENGAGEMENT',
            ),
          ),
          5 =>
          array(
            0 => array(
              'name' => 'date_effet',
              'label' => 'LBL_DATE_EFFET',
            ),
            1 => '',
          ),
          6 =>
          array(
            0 => 'date_entered',
            1 => 'date_modified',
          ),
        ),
      ),
    ),
  );;
