<?php


if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

global $mod_strings, $app_strings, $sugar_config;


if (ACLController::checkAccess('OPS_dossier', 'edit', true)) {
    $module_menu[]    =    array(
        'index.php?module=OPS_dossier&action=choix_dispositif&return_module=OPS_dossier&return_action=DetailView',
        "Créer un dossier",
        'Add',
        'OPS_dossier'
    );
}

if (ACLController::checkAccess('OPS_dossier', 'list', true)) {
    $module_menu[]    =    array(
        'index.php?module=OPS_dossier&action=index&return_module=OPS_dossier&return_action=DetailView',
        $mod_strings['LNK_LIST'],
        'View',
        'OPS_dossier'
    );
}

if (ACLController::checkAccess('OPS_dossier', 'edit', true)) {
    $module_menu[]    =    array(
        'index.php?module=OPS_dossier&action=astre_import&return_module=OPS_dossier&return_action=DetailView',
        "Mise à jour Astre",
        'Import',
        'OPS_dossier'
    );
}

/*// Logic Code - Menu Geoloc [Implement GEOLOC » ODE CORE]
if (ACLController::checkAccess('OPS_dossier', 'list', true)) {
	$module_menu[]	=	array(
		'index.php?module=OPS_dossier&action=geoloc&return_module=OPS_dossier&return_action=DetailView',
		$mod_strings['LBL_OPS_GEOLOC_BTN_ACTION'],
		'View',
		'OPS_dossier'
	);
}
*/