<div role="ode-modal-page" data-route="documents" class="ode-modal-page">
    <div>
        <table id="ode-appairage-table" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Num dossier</th>
                    <th>Nom document</th>
                    <th>Type</th>
                    <th>Format</th>
                    <th>Taille </th>
                    <th>Télécharger</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th>Num dossier</th>
                    <th>Nom document</th>
                    <th>Type</th>
                    <th>Format</th>
                    <th>Taille </th>
                    <th>Télécharger</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
