<div role="ode-modal-page" data-route="engagements" class="ode-modal-page">
    <div>
        <table id="ode-appairage-table" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Num engagement</th>
                    <th>Num dossier</th>
                    <th>Statut</th>
                    <th>Montant engagé</th>
                    <th>Montant liquidé </th>
                    <th>Montant à liquider</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th>Num engagement</th>
                    <th>Num dossier</th>
                    <th>Statut</th>
                    <th>Montant engagé</th>
                    <th>Montant liquidé </th>
                    <th>Montant à liquider</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
