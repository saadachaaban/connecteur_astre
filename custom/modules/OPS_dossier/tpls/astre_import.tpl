
<link href="custom/include/css/spinner.css" rel="stylesheet" type="text/css" />
<link href="custom/include/Ode/Modal/css/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="custom/modules/OPS_dossier/css/astre_import.css" rel="stylesheet" type="text/css" /> 

<script type="text/javascript" charset="UTF-8" src="custom/include/helpers/ode_helper.js"></script>
<script type="text/javascript" charset="UTF-8" src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
<script type="text/javascript" charset="UTF-8" src="custom/include/Ode/Modal/js/datatables.min.js"></script>
<script type="text/javascript" charset="UTF-8" src="custom/include/Ode/Helper/OdeQueriesHelper.js"></script>
<script type="text/javascript" charset="UTF-8" src="custom/include/Ode/Helper/OdeAjaxHelper.js"></script>
<script type="text/javascript" charset="UTF-8" src="custom/modules/OPS_dossier/js/astre_import.js"></script>


<input type="hidden" value="" id="import_file_content">

<div style="min-height: 660px;">
 
    <h1> 
        <span class="glyphicon glyphicon-import"></span>
         Mise à jour des informations Astre
    </h1>

    <form style=" margin-left: 5%;">

        <div style="position: relative; width: 20%; display: inline-block;">  
            <input style="opacity: 0;" id="import_file" type="file">
            <label tabindex="0" for="import_file" style="display: block;padding: 14px 45px;background: #35B3C5;color: #fff;font-size: 1.2em;transition: all .4s;cursor: pointer;border-radius: 5px;">Séléctionner un fichier (.txt) </label>
        </div>

        <p id="import_file_name" style="font-style: italic;  display: inline-block;  margin-left: 11px; font-weight: bold; width: 20%;"> Aucun fichier séléctionné </p>
        
        <div id="import_file_buttons" style="display: inline-block; width: 20%; margin-left: 6%;"> 
            <input title="Soumettre" type="button" id="ode_import_soumettre_btn" value="Soumettre" disabled="disabled" style="">
        </div>

    </form>

    <div id="onglets">

        <ul id="onglet-boutons">
            <li><a id="bouton_fichier" class="active" >Fichier</a></li>
            <li><a id="bouton_dossiers" class="">Informations Astre</a></li>
        </ul>

        <div id="onglet-content">
            <div id="onglet_ficher">
                <p>Aucun fichier séléctionné</p>
            </div>
            <div id="onglet_dossiers" data-ready="false" style="display: none;">
                <div style="margin: 2%;">
                    <table id="ode-import-table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>Dossier id</th>
                                <th>Engagement de CP</th>
                                <th>N° liquidation</th>
                                <th>Date liquidation</th>
                                <th>Montant liquidé</th>
                                <th>N° mandatement</th>
                                <th>Date mandatement</th>
                                <th>Montant mandaté</th>
                                <th>N° de bordereau</th>
                                <th>Date paiement</th>
                                <th>Montant payé</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                            <tr>
                                <th>Dossier id</th>
                                <th>Engagement de CP</th>
                                <th>N° liquidation</th>
                                <th>Date liquidation</th>
                                <th>Montant liquidé</th>
                                <th>N° mandatement</th>
                                <th>Date mandatement</th>
                                <th>Montant mandaté</th>
                                <th>N° de bordereau</th>
                                <th>Date paiement</th>
                                <th>Montant payé</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <input title="Mettre à jour" type="button" id="ode_import_update_btn" value="Mettre à jour" disabled="disabled" style="float: right;margin-right: 2%;">
            </div>
        </div>
        
    </div>

</div>
