String.prototype.b64ToUtf8 = function() {
    return decodeURIComponent(escape(window.atob( this )));
};

if( typeof(AstreImport) == 'undefined' ){ var AstreImport = { bouton_id: 'ode_btn_importer', loaded: false }; }


$(document).ready(function() {
    AstreImport.setOnChangeFile();
    AstreImport.setOnClickSoumettre();
    AstreImport.setOnClickOngletBoutons();
});
 
AstreImport.setOnChangeFile = function(){
    $("#import_file").change(function(e){
        $('#onglet_ficher').empty();
        $('#onglet_dossiers').attr('data-ready','false');
        $('#bouton_fichier').click();
        var statut = false;
        var file = e.target.files[0];
        if( typeof file.name === "string" && file.name !== "" ){
            $("#import_file_name").text( file.name )
            if( file.type === "text/plain" ){
                var selected_file = document.getElementById('import_file').files[0];
                var content_file = document.getElementById('import_file_content');
                const reader = new FileReader();
                reader.onload = function(event) { 
                    content_file.value = utf8_to_b64(reader.result);           
                    $('#onglet_ficher').append('<pre>'+reader.result +'</pre>');
                };
                reader.readAsText(selected_file);
                statut = true;
            }else{
                $('#onglet_ficher').append('<p>Le fichier n\'est pas au format .txt </p>');
                console.log(" Ce n'est pas un text/plain ");
            }
        }else{
            $('#onglet_ficher').append('<p>Aucun fichier séléctionné</p>');
            $("#import_file_name").text( default_file_name )
        }
        
        $("#import_file_buttons :input").each(function() {
            ( statut === false ) ? $(this).attr( "disabled", "disabled" ) : $(this).removeAttr( "disabled" ) ;
        });
    });
}


AstreImport.setOnClickOngletBoutons = function(){
    $('#bouton_fichier').off().on('click', function () {
        $('#bouton_fichier').removeClass().addClass('active');
        $('#bouton_dossiers').removeClass();
        $('#onglet_ficher').show();
        $('#onglet_dossiers').hide();
    });
    $('#bouton_dossiers').off().on('click', function () {
        if ($('#onglet_dossiers').attr('data-ready') === 'true' ) {
            $('#bouton_dossiers').removeClass().addClass('active');
            $('#bouton_fichier').removeClass();
            $('#onglet_dossiers').show();
            $('#onglet_ficher').hide();
        }
    });
}

AstreImport.setOnClickSoumettre = function(){
    $('#ode_import_soumettre_btn').off().on('click', function () {

        var loading = function(){
            $.LoadingOverlay("show");
        };
    
        var callBack = function(){

            if ( this.statut === false ) {
                alert(this.result);
            } else {

                $('#onglet_dossiers').attr('data-ready','true');
                $(`#ode-import-table tbody`).empty();

                $.each(this.result, function(index, ligne) {
                    var ligne_html = `<tr>`;
                    ligne_html +=       `<td> ${ligne.dossier_id} </td>`;
                    ligne_html +=       `<td> ${ligne.engagement_num} </td>`;
                    ligne_html +=       `<td> ${ligne.liquidation_num} </td>`;
                    ligne_html +=       `<td> ${ligne.liquidation_date} </td>`;
                    ligne_html +=       `<td> ${ligne.liquidation_montant} </td>`;
                    ligne_html +=       `<td> ${ligne.mandat_num} </td>`;
                    ligne_html +=       `<td> ${ligne.mandat_date} </td>`;
                    ligne_html +=       `<td> ${ligne.mandat_montant} </td>`;
                    ligne_html +=       `<td> ${ligne.paiement_num} </td>`;
                    ligne_html +=       `<td> ${ligne.paiement_date} </td>`;
                    ligne_html +=       `<td> ${ligne.paiement_montant} </td>`;
                    ligne_html += `</tr>`;
                    $(`#ode-import-table tbody`).append(ligne_html);
                });
        
                AstreImport.initDataTable();
                AstreImport.setOnClickUpdate();

                if ( $(`#ode-import-table tbody`).children().length > 0 ) {
                    $('#ode_import_update_btn').removeAttr("disabled");
                } else {
                    $('#ode_import_update_btn').attr("disabled","disabled");
                }
            } 

            setTimeout(function(){
                $.LoadingOverlay("hide");
                $('#bouton_dossiers').click();
            }, 1000);
        };
    
        var file_content =  $("#import_file_content").val();
        var query = OdeQueries.getAjaxActionQuery();
        query.setModule('OPS_dossier');
        query.setAction("getVisualisationFileImported");
        query.setPostData({file_content: file_content});
        OdeAjax.getByAction( query, loading, callBack );
    });
}

AstreImport.initDataTable = function(){ 
    const table = $(`#ode-import-table`);
    if ( table.length > 0 && $.fn.dataTable.isDataTable(table) === false) {
        table.DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.11.5/i18n/fr-FR.json"
            }
        });
    }
}

AstreImport.setOnClickUpdate = function(){
    $('#ode_import_update_btn').off().on('click', function () {

        var loading = function(){
            $.LoadingOverlay("show");
        };
    
        var callBack = function(){
            if ( this.statut === false ) return; 
            console.log("updateDossierFileImported THis result =", this.result)
            setTimeout(function(){
                $.LoadingOverlay("hide");
            }, 1000);
        };
    
        var file_content =  $("#import_file_content").val();
        var query = OdeQueries.getAjaxActionQuery();
        query.setModule('OPS_dossier');
        query.setAction("updateDossierFileImported");
        query.setPostData({file_content: file_content});
        OdeAjax.getByAction( query, loading, callBack );
    });
}
