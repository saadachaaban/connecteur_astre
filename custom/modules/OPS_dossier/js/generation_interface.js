String.prototype.b64ToUtf8 = function() {
    return decodeURIComponent(escape(window.atob( this )));
};

// We create an instance of the OdeModal & OdeTable
const ModalGenerationInterface = Object.create(OdeModal);

if( typeof(GenerationInterface) == 'undefined' ){ var GenerationInterface = { modal_id: 'ode_modal_generation_interface', loaded: false }; }

// We initialize the onclick function [ Action par lot ]
GenerationInterface.setClickActionLotBtn = function(){
    $('[role="ode-modal-generation-interface-btn"]').off().on('click', function () {
        ModalGenerationInterface.open();
        GenerationInterface.initialisation();
    });
}

// The initialization function is the first function triggered when the modal is opened.
GenerationInterface.initialisation = function(){ 

    ModalGenerationInterface.route('loading');

    var loading = function(){
        ModalGenerationInterface.loading.setMessage('Récupération des dossiers séléctionnés');
    };

    var callBack = function(){

        if ( this.statut === false ) return; 
        
        var message = ( this.result.length === 1 ) 
                    ? `Récupération des engagements du dossier séléctionné`
                    : `Récupération des engagements des ${this.result.length} dossiers séléctionnés`;

        ModalGenerationInterface.loading.setMessage(message);
        ModalGenerationInterface.loading.setTotal(this.result.length);

        $(`#${ModalGenerationInterface.modal_id} [role="ode-modal-page"][data-route="engagements"] tbody`).empty();

        $.each(this.result, function(key, dossier_id) {
            GenerationInterface.getDossierEngagements(dossier_id);
        });

    };

    var dossiers = ModalGenerationInterface.getSelectedDossiers();

    var query = OdeQueries.getAjaxActionQuery();
    query.setModule('OPS_dossier');
    query.setAction("getDossiers");
    query.setPostData( dossiers );
    OdeAjax.getByAction( query, loading, callBack );

}

GenerationInterface.getDossierEngagements = function( dossier_id ){

    var loading = function(){};

    var callBack = async function(){

        if ( this.statut === false ) return; 

        ModalGenerationInterface.loading.increaseCurrent();
        ModalGenerationInterface.loading.setMessage(`Vérification du dossier n°${this.result.num} (${ModalGenerationInterface.loading.current}/${ModalGenerationInterface.loading.total})`);

        if ( this.result.statut === true ) {
            $.each(this.result.engagements, function(key, engagement) {
                var ligne_html = GenerationInterface.getLigne(engagement);
                $(`#${ModalGenerationInterface.modal_id} [role="ode-modal-page"][data-route="engagements"] tbody`).append(ligne_html);
            });
        }

        if ( ModalGenerationInterface.loading.current === ModalGenerationInterface.loading.total ){
            ModalGenerationInterface.initDataTable('engagements');
            ModalGenerationInterface.boutons.empty();
            GenerationInterface.addBoutonGenerer();
            GenerationInterface.setInformation();
            await ModalGenerationInterface.sleep(1500);
            ModalGenerationInterface.boutons.show();
            ModalGenerationInterface.route('engagements');
        }
        
    };

    var query = OdeQueries.getAjaxActionQuery();
    query.setModule('OPS_dossier');
    query.setAction("getEngagements");
    query.setPostData(dossier_id);
    OdeAjax.getByAction( query, loading, callBack );

} 

GenerationInterface.getLigne = function( engagement ){    

    var title = ( typeof engagement.dossier_name === 'string' && engagement.dossier_name !== '') ? `title="${engagement.dossier_name}"` : ''; 
    var ligne = `<tr data-id="${engagement.id}" data-dossier-id="${engagement.dossier_id}" data-statut="${engagement.statut}" data-montant-liquider="${engagement.montant_a_liquider}" >`;

    ligne    +=    `<td ><a target="_blank" href="/index.php?module=OPS_engagement&action=DetailView&record=${engagement.id}"> ${engagement.name} </a></td>`;
    ligne    +=    `<td ${title} ><a target="_blank" href="/index.php?module=OPS_dossier&action=DetailView&record=${engagement.dossier_id}"> ${engagement.dossier_num} </a></td>`;
    ligne    +=     GenerationInterface.getIcone(engagement.statut);
    ligne    +=     `<td> ${parseFloat(engagement.montant_engage).toFixed(2)}€ </td>`
    ligne    +=     `<td> ${parseFloat(engagement.montant_liquide).toFixed(2)}€ </td>`
    ligne    +=     `<td> ${parseFloat(engagement.montant_a_liquider).toFixed(2)}€ </td>`
    ligne    += `</tr>`;

    return ligne;
}

GenerationInterface.getIcone = function( statut ){
    if(statut){
       return  `<td> <i class="fa-solid fa-circle-check ode-modal-icon-success"></i> </td>`;
    }else{
        return  `<td> <i class="fa-solid fa-circle-xmark ode-modal-icon-error"></i> </td>`;
    }
}

GenerationInterface.setInformation = function(){

    const engagement_valid = GenerationInterface.getCountEngagement('true');
    const engagement_invalid = GenerationInterface.getCountEngagement('false');

    var message = '';
    if ( engagement_valid === 0 && engagement_invalid === 0 ) {
        message = 'Aucun engagement';
    } else {
        if ( engagement_valid === 1 ) {
            message += `Un seul engagement à liquider`;
        } else if( engagement_valid > 1 ){
            message += `${engagement_valid} engagements à liquider`;
        }
        if ( engagement_invalid === 1 ) {
            message += ( engagement_valid > 0 ) ? ` et un seul engagement invalide` : `Un seul engagement invalide`;
        } else if( engagement_invalid > 1 ){
            message += ( engagement_valid > 0 ) ? ` et ${engagement_invalid} engagements invalides` : `${engagement_invalid} engagements invalides`;
        }
    }

    ModalGenerationInterface.information.add(message);
    ModalGenerationInterface.information.show();
}

GenerationInterface.getCountEngagement = function(statut){
    return $(`#${ModalGenerationInterface.modal_id} [role="ode-modal-page"][data-route="engagements"] tbody tr[data-statut="${statut}"]`).length;
}

GenerationInterface.addBoutonGenerer = function(){

    const engagements = GenerationInterface.getEngagements();

    if ( engagements.length > 0 ) {

        ModalGenerationInterface.boutons.add('Générer le fichier' ,{role: 'ode-modal-generer-btn'});
        ModalGenerationInterface.boutons.show();

        $('[role="ode-modal-generer-btn"]').off().on('click', function () {
    
            ModalGenerationInterface.loading.reset();
            ModalGenerationInterface.loading.setMessage('Génération du fichier d\'interface');
            ModalGenerationInterface.loading.setTotal(1);

            var loading = function(){
                ModalGenerationInterface.route('loading');
            };

            var callBack = async function(){

                if ( this.statut === false ) return;

                ModalGenerationInterface.loading.increaseCurrent();

                await ModalGenerationInterface.sleep(1000);
                ModalGenerationInterface.route('engagements');
                GenerationInterface.downloadFile('fichier_interface.txt', this.result.b64ToUtf8() );
            };
            
            var query = OdeQueries.getAjaxActionQuery();
            query.setModule('OPS_dossier');
            query.setAction("genererLiquidationFile");
            query.setPostData({engagements: engagements});
            OdeAjax.getByAction( query, loading, callBack );
        });

    }
}

GenerationInterface.getEngagements = function(){
    var engagements = [];
    const lignes = $(`#${ModalGenerationInterface.modal_id} [role="ode-modal-page"][data-route="engagements"] tr`);
    if ( lignes.length > 0 ){
        lignes.each(function() {
            if ( $(this).attr('data-id') !== undefined && $(this).attr('data-id') !== "" && $(this).attr('data-statut') === "true" ){
                engagements.push({ 
                    dossier_id: $(this).attr('data-dossier-id'),
                    engagement_id: $(this).attr('data-id'),
                    montant_a_liquider: $(this).attr('data-montant-liquider')
                }); 
            }
        });
    };
    return engagements;
}

GenerationInterface.downloadFile = function(filename, text){
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}

$(document).ready(function() {
    ModalGenerationInterface.initialisation( GenerationInterface.modal_id );
    if ( ModalGenerationInterface.loaded ) {
        GenerationInterface.setClickActionLotBtn();
    }
});
