
// We create an instance of the OdeModal & OdeTable
const ModalEnvoieDocument = Object.create(OdeModal);

if( typeof(EnvoieDocument) == 'undefined' ){ var EnvoieDocument = { modal_id: 'ode_modal_envoie_document', loaded: false }; }

// We initialize the onclick function [ Action par lot ]
EnvoieDocument.setClickActionLotBtn = function(){
    $('[role="ode-modal-envoie-document-btn"]').off().on('click', function () {
        ModalEnvoieDocument.open();
        EnvoieDocument.initialisation();
    });
}

// The initialization function is the first function triggered when the modal is opened.
EnvoieDocument.initialisation = function(){ 

    ModalEnvoieDocument.route('loading');

    var loading = function(){
        ModalEnvoieDocument.loading.setMessage('Récupération des dossiers séléctionnés');
    };

    var callBack = function(){ 

        if ( this.statut === false ) return; 
        
        var message = ( this.result.length === 1 ) 
                    ? `Récupération des documents du dossier séléctionné`
                    : `Récupération des documents des ${this.result.length} dossiers séléctionnés`;

        ModalEnvoieDocument.loading.setMessage(message);
        ModalEnvoieDocument.loading.setTotal(this.result.length);

        $(`#${ModalEnvoieDocument.modal_id} [role="ode-modal-page"][data-route="documents"] tbody`).empty();

        $.each(this.result, function(key, dossier_id) {
            EnvoieDocument.getJustificatifs(dossier_id);
        });

    };

    var dossiers = ModalEnvoieDocument.getSelectedDossiers();

    var query = OdeQueries.getAjaxActionQuery();
    query.setModule('OPS_dossier');
    query.setAction("getDossiers");
    query.setPostData( dossiers );
    OdeAjax.getByAction( query, loading, callBack );

}

EnvoieDocument.getJustificatifs = function( dossier_id ){

    var loading = function(){};

    var callBack = async function(){

        if ( this.statut === false ) return; 

        ModalEnvoieDocument.loading.increaseCurrent();
        ModalEnvoieDocument.loading.setMessage(`Vérification du dossier n°${this.result.num} (${ModalEnvoieDocument.loading.current}/${ModalEnvoieDocument.loading.total})`);

        if ( this.result.statut === true ) {
            $.each(this.result.justificatifs, function(key, justificatif) {
                var ligne_html = EnvoieDocument.getLigne(justificatif);
                $(`#${ModalEnvoieDocument.modal_id} [role="ode-modal-page"][data-route="documents"] tbody`).append(ligne_html);
            });
        }

        if ( ModalEnvoieDocument.loading.current === ModalEnvoieDocument.loading.total ){
            ModalEnvoieDocument.initDataTable('documents');
            ModalEnvoieDocument.boutons.empty();
            await ModalEnvoieDocument.sleep(1500);
            ModalEnvoieDocument.route('documents');
        }
        
    };

    var query = OdeQueries.getAjaxActionQuery();
    query.setModule('OPS_dossier');
    query.setAction("getJustificatifs");
    query.setPostData(dossier_id);
    OdeAjax.getByAction( query, loading, callBack );

} 

EnvoieDocument.getLigne = function( justificatif ){    

    var title = ( typeof justificatif.dossier_name === 'string' && justificatif.dossier_name !== '') ? `title="${justificatif.dossier_name}"` : ''; 
    var format_style = ( justificatif.format_valid === true ) ? '' : ' style="color: #dc3545;" '
    var taille_style = ( justificatif.taille_valid === true ) ? '' : ' style="color: #dc3545;" '
    var ligne = `<tr data-id="${justificatif.id}" data-dossier-id="${justificatif.dossier_id}" >`;
    ligne    +=    `<td ${title} ><a target="_blank" href="/index.php?module=OPS_dossier&action=DetailView&record=${justificatif.dossier_id}"> ${justificatif.dossier_num} </a></td>`;
    ligne    +=    `<td ><a target="_blank" href="/index.php?module=OPS_justificatif&action=DetailView&record=${justificatif.id}"> ${justificatif.name} </a></td>`;
    ligne    +=     `<td> ${justificatif.type} </td>`
    ligne    +=     `<td ${format_style}> ${justificatif.format} </td>`
    ligne    +=     `<td ${taille_style}> ${justificatif.taille_display} </td>`
    ligne    +=     `<td>`
    if( justificatif.taille > 0 ) {
        ligne    +=     `<a href="index.php?entryPoint=download_justificatif&id=${justificatif.id}&type=OPS_justificatif" target="_blank"><i class="fa-solid fa-download" style="color: #35b3c5; font-size: 20px;"></i>`
    } else {
        ligne    +=     `<i class="fa-solid fa-circle-xmark" style="color: #dc3545; font-size: 20px;"></i>`
    }            
    ligne    +=    `</td>`
    ligne    += `</tr>`;

    return ligne;
}

$(document).ready(function() {
    ModalEnvoieDocument.initialisation( EnvoieDocument.modal_id );
    if ( ModalEnvoieDocument.loaded ) {
        EnvoieDocument.setClickActionLotBtn();
    }
});
