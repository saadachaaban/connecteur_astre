<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');


class ViewAstre_import extends SugarView
{

    public function __construct($bean = null, $view_object_map = array())
    {
        parent::SugarView($bean, $view_object_map);
    }

    public function preDisplay()
    {
        if (!$this->bean->ACLAccess('edit')) {
            ACLController::displayNoAccess();
            sugar_die('Access Denied Generateur vue');
        }
    }

    /**
     * @see SugarView::display()
     */
    public function display()
    {
        $smarty = new Sugar_Smarty();
        $current_template = "custom/modules/OPS_dossier/tpls/astre_import.tpl";
        $smarty->display($current_template);
    }
}
