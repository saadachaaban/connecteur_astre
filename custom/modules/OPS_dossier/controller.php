<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once 'modules/OPS_dossier/controller.php';
require_once 'include/MassUpdate.php';

use ODE\Helper\OdeArrayHelper;
use Astre\Entity\DossierEntity;
use Astre\Controller\InterfacageController;
use Astre\Controller\FileImportController;

class CustomOPS_dossierController extends OPS_dossierController
{

    /********************************************************************************************************************/
    /************************************** Modale Génération fichier d'interface  **************************************/
    /********************************************************************************************************************/

    /**
     * @access public
     * action_getDossiers()
     * Fonction qui retourne les ids des dossiers séléctionnés
     * 
     * @return json     $data
     */
    public function action_getDossiers()
    {
        global $db;
        $libelle_erreur = '';
        $dossiers = [];

        do {

            $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);

            if ($data_verified['statut'] === "err") {
                $libelle_erreur = 'Aucun dossier n\'a été récupéré';
                break;
            }

            $data = $data_verified["data"]["json_array"];

            if (!empty($data['dossiers']) && is_array($data['dossiers']) && count($data['dossiers']) > 0) {
                $dossiers = $data['dossiers'];
                break;
            }

            if (!empty($data['filtres'])) {
                // Ce traitement est utilisé dans le cas ou l'utilisateur sélectionne l'intégralité des dossiers + filtres
                $mass = new MassUpdate();
                $mass->generateSearchWhere('OPS_dossier', $data['filtres']);
                $where_clause = $mass->where_clauses;
                $obj_dossier = new OPS_dossier();
                $query = $obj_dossier->create_export_query("", $where_clause);
                $result = $db->query($query);
                while ($dossier = $db->fetchByAssoc($result)) {
                    $dossiers[] = $dossier['id'];
                }
            }

            // Je ne vérifie pas si $request['selectAll'] est non vide, empty($request['selectAll']) retourne true quand $request['selectAll']=0
            if (!is_array($dossiers) || count($dossiers) === 0) {
                $libelle_erreur = 'Aucun dossier n\'a été récupéré';
                break;
            }
        } while (0);

        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => $dossiers) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }

    /**
     * @access public
     * action_getEngagements()
     * Fonction qui retoune les engagements du dossier
     * 
     * @return json     $data
     */
    public function action_getEngagements()
    {
        $dossier = [];
        $libelle_erreur = "";
        do {

            $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);
            if ($data_verified['statut'] === "err") {
                $libelle_erreur = 'Aucun dossier n\'a été récupéré';
                break;
            }

            $dossier_id = (!empty($data_verified["data"]["json_array"])) ? $data_verified["data"]["json_array"] : '';
            if (!empty($dossier_id)) {
                $dossierEntity = new DossierEntity($dossier_id);
                $dossier = $dossierEntity->getEngagements();
            }
        } while (0);

        //sleep(2);
        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => $dossier) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }


    /**
     * @access public
     * action_genererLiquidationFile()
     * Fonction qui genere le fichier de liquidation
     * 
     * @return json     $data
     */
    public function action_genererLiquidationFile()
    {
        $libelle_erreur = "";
        do {

            $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);

            if ($data_verified['statut'] === "err") {
                $libelle_erreur = 'Aucun engagement n\'a été récupéré';
                break;
            }

            $engagements = (!empty($data_verified["data"]["json_array"]['engagements'])) ? $data_verified["data"]["json_array"]['engagements'] : '';
            if (empty($engagements)) {
                $libelle_erreur = 'Erreur de récupération des engagements';
                break;
            }

            $InterfacageCtlr = new InterfacageController();
            $file = $InterfacageCtlr->getFileContent($engagements);

            if ($file->statut === false) {
                $libelle_erreur = 'Erreur de construction du fichier';
                break;
            }
            /*
            $GLOBALS['log']->fatal(" -> action_genererLiquidationFile" . print_r([
                'engagements' => $engagements,
                'file' => $file,
            ], true));
            */
        } while (0);

        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => base64_encode($file->data)) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }

    /********************************************************************************************************************/
    /******************************************* Action : Import d'un fichier  ******************************************/
    /********************************************************************************************************************/

    /**
     * @access public
     * action_getVisualisationFileImported()
     * Fonction qui retourne la visualisation du fichier de mise à jour des informations Astre
     * 
     * @return json     $data
     */
    public function action_getVisualisationFileImported()
    {
        $file_data = [];
        $libelle_erreur = "";
        do {

            // On récupére les données Ajax
            $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);
            if ($data_verified['statut'] === "err") {
                $libelle_erreur = 'Erreur de récupération du fichier';
                break;
            }

            // On récupére le contenu du fichier transmis par l'ajax
            $file_content = (!empty($data_verified["data"]["json_array"]['file_content'])) ? $data_verified["data"]["json_array"]['file_content'] : '';
            if (empty($file_content)) {
                $libelle_erreur = 'Erreur de récupération du fichier';
                break;
            }

            // Mapping du fichier de mise à jour des informations Astre
            $mapping = ['dossier_id', 'engagement_num', 'liquidation_num', 'liquidation_date', 'liquidation_montant', 'mandat_num', 'mandat_date', 'mandat_montant', 'paiement_num', 'paiement_date', 'paiement_montant'];

            // On récupére les données du fichier en tableau
            $FileImportCtrl = new FileImportController();
            $file_data = $FileImportCtrl->getDataFromFile($file_content, $mapping);
            if (!is_array($file_data) || count($file_data) === 0) {
                $libelle_erreur = 'Erreur de récupération des champs';
                break;
            }
        } while (0);

        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => $file_data) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }


    /**
     * @access public
     * action_updateDossierFileImported()
     * Fonction qui met à jour les informations Astre depuis un fichier
     * 
     * @return json     $data
     */
    public function action_updateDossierFileImported()
    {
        $donnees = [];
        $libelle_erreur = "";
        do {

            $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);

            if ($data_verified['statut'] === "err") {
                $libelle_erreur = 'Erreur de récupération du fichier';
                break;
            }

            $file_content = (!empty($data_verified["data"]["json_array"]['file_content'])) ? $data_verified["data"]["json_array"]['file_content'] : '';
            if (empty($file_content)) {
                $libelle_erreur = 'Erreur de récupération du fichier';
                break;
            }

            // On récupére les données du fichier en tableau
            $FileImportCtrl = new FileImportController();
            $FileImportCtrl->updateInformationAstre($file_content);
        } while (0);

        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => $donnees) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }


    /********************************************************************************************************************/
    /******************************************* Action : Envoie de documents  ******************************************/
    /********************************************************************************************************************/

    /**
     * @access public
     * action_getJustificatifs()
     * Fonction qui retourne les documents du dossiers
     * 
     * @return json     $data
     */
    public function action_getJustificatifs()
    {
        $dossier = [];
        $libelle_erreur = "";
        do {

            $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);

            if ($data_verified['statut'] === "err") {
                $libelle_erreur = 'Aucun dossier n\'a été récupéré';
                break;
            }

            $dossier_id = (!empty($data_verified["data"]["json_array"])) ? $data_verified["data"]["json_array"] : '';
            if (!empty($dossier_id)) {
                $dossierEntity = new DossierEntity($dossier_id);
                $dossier = $dossierEntity->getJustificatifs();
            }
        } while (0);

        //sleep(2);
        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => $dossier) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }
}
