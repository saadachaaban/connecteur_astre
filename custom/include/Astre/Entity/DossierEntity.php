<?php

namespace Astre\Entity;

use BeanFactory;
use ODE\Service\DossierService;
use ODE\Service\JournalService;

/**
 * Class DossierEntity
 *
 * @package Astre\Entity
 */
class DossierEntity
{

    /** @var bool */
    public $statut = false;

    /** @var string */
    public $id = '';

    /** @var string */
    public $name = '';

    /** @var string */
    public $num = '';

    /** @var array */
    public $engagements = [];

    /** @var array */
    public $justificatifs = [];

    /**
     * DossierEntity constructor.
     * @param string $dossier_id
     */
    public function __construct($dossier_id = '')
    {

        do {

            // On vérifie que l'id du dossier n'est pas vide
            if (empty($dossier_id)) break;

            // On récupere le dossier
            $dossier = DossierService::get($dossier_id);
            if ($dossier === false) break;

            $this->id = $dossier->id;
            $this->name = $dossier->libelle_dossier;
            $this->num = $dossier->num_dossier;
            $this->statut = true;

            // On récupére les engagements du dossier
            $this->engagements = DossierService::getEngagements($this->id);

            // On récupére les justificatifs ops_justificatif
            $this->justificatifs = DossierService::getJustificatifs($this->id);
        } while (0);
    }

    /**
     * Fonction qui retourne la vérification des engagements du dossier
     * @return object
     */
    public function getEngagements()
    {

        $engagements = [];

        if (count($this->engagements) > 0) {

            foreach ($this->engagements as $engagement) {

                $montant_ttc = (number_format((float)$engagement->montant_ttc, 2, '.', '') > 0) ? number_format((float)$engagement->montant_ttc, 2, '.', '') : 0.00;
                $montant_liquide = (number_format((float)$engagement->montant_liquide, 2, '.', '') > 0) ? number_format((float)$engagement->montant_liquide, 2, '.', '') : 0.00;
                $montant_a_liquider = (($montant_ttc - $montant_liquide) > 0) ? $montant_ttc - $montant_liquide : 0.00;
                $statut = ($montant_a_liquider > 0) ? true : false;

                $engagements[] = (object) [
                    'statut' => $statut,
                    'id' => $engagement->id,
                    'name' => $engagement->name,
                    'dossier_id' => $this->id,
                    'dossier_name' => $this->name,
                    'dossier_num' => $this->num,
                    'montant_engage' => $montant_ttc,
                    'montant_liquide' => $montant_liquide,
                    'montant_a_liquider' => $montant_a_liquider
                ];
            }
        }

        return (object) [
            'statut' => (is_array($engagements) && count($engagements) > 0) ? true : false,
            'num' => $this->num,
            'engagements' => $engagements
        ];
    }

    /**
     * Fonction qui retourne les justificatifs du dossiers
     * @return object
     */
    public function getJustificatifs()
    {

        $justificatifs = [];

        if (count($this->justificatifs) > 0) {

            foreach ($this->justificatifs as $justificatif) {

                $justificatifs[] = (object) [
                    'id' => $justificatif->id,
                    'name' => $justificatif->document_name,
                    'type' => $justificatif->ops_type_document_name,
                    'format' => $justificatif->file_mime_type,
                    'taille' => $justificatif->file_size,
                    'taille_display' => $justificatif->file_size_display,
                    'dossier_id' => $this->id,
                    'dossier_name' => $this->name,
                    'dossier_num' => $this->num,
                    'format_valid' => $this->isFormatJustificatifValid($justificatif->file_mime_type),
                    'taille_valid' => $this->isTailleJustificatifValid($justificatif->file_size),
                ];
            }
        }

        return (object) [
            'statut' => (is_array($justificatifs) && count($justificatifs) > 0) ? true : false,
            'num' => $this->num,
            'justificatifs' => $justificatifs
        ];
    }

    /**
     * Fonction qui vérifie si le format de la piece justificative est conforme à la config Astre 
     * @return object
     */
    public function isFormatJustificatifValid($type = '')
    {
        global $sugar_config;
        $types = !empty($sugar_config['opensub']['astre']['document_types']) ? array_map('trim', explode(';', $sugar_config['opensub']['astre']['document_types'])) : [];
        // Si la config est vide on considere que tous les formats sont acceptés
        if (count($types) === 0) return true;
        // Sinon on vérifie si le type figure bien parmis les types acceptés
        return !empty($type) && in_array(trim($type), $types) ? true : false;
    }

    /**
     * Fonction qui vérifie si la taille de la piece justificative est conforme à la config Astre 
     * @return object
     */
    public function isTailleJustificatifValid($taille = 0)
    {
        global $sugar_config;
        $max_size = !empty($sugar_config['opensub']['astre']['document_max_size']) ? intval($sugar_config['opensub']['astre']['document_max_size']) : 0;
        // Si la config est vide on considere que il n'y'a pas de limite au fichier
        if ($max_size === 0) return true;
        // Sinon on vérifie si la taille est inférieure à la taille max acceptée ( *1024 pour avoir le nombre d'octets, la config est en ko )
        return intval($taille) > 0 && $taille <= $max_size * 1024 ? true : false;
    }

    /**
     * Fonction qui retourne l'initialisation du detail d'un journal
     * @return object
     */
    public function getJournalDetail($name = '', $tache = '')
    {
        return (object) [
            'name' => $name,
            'tache' => $tache,
            'reponse' => 'Non',
            'statut' => 'err',
            'erreur' => '',
            'description' => '',
        ];
    }

    /**
     * Fonction qui retourne l'initialisation du detail d'un journal
     * @param object
     */
    public function setJournalDetails($details = [])
    {
        if ($this->tiers !== false && $this->tiers_appairage !== false) {

            // Récupération ou création du journal si il n'existe pas
            if ($this->tiers_appairage_journal !== false) {
                $obj_journal = BeanFactory::getBean('OPS_journal', $this->tiers_appairage_journal->id);
            } else {
                $obj_journal = BeanFactory::newBean('OPS_journal');
                $obj_journal->name = 'Journal Appairage Tiers : ' . $this->tiers->name_display;
                $obj_journal->parent_type = 'OPS_appairage';
                $obj_journal->parent_id = $this->tiers_appairage->id;
                $obj_journal->statut = $this->tiers_appairage->statut;
                if ($obj_journal->save()) {
                    $this->tiers_appairage_journal = JournalService::get($obj_journal->id);
                }
            }

            if (!empty($obj_journal->id) && is_array($details) && count($details) > 0) {
                JournalService::deleteDetails($obj_journal->id);
                $obj_journal->load_relationship('ops_journal_detail_ops_journal');
                foreach ($details as $key => $detail) {
                    $obj_detail = BeanFactory::newBean('OPS_journal_detail');
                    $obj_detail->name = $detail->name;
                    $obj_detail->tache = $detail->tache;
                    $obj_detail->reponse = $detail->reponse;
                    $obj_detail->description = $detail->description;
                    $obj_detail->statut = $detail->statut;
                    $obj_detail->erreur = $detail->erreur;
                    $obj_detail->ordre = $key + 1;
                    $obj_detail->save();
                    $obj_journal->ops_journal_detail_ops_journal->add($obj_detail);
                }
            }
        }
    }
}
