<?php


namespace Astre\Controller;

use DateTime;
use BeanFactory;

use Astre\Interfacage\InterfacageLiquidation;


/**
 * Class InterfacageController
 * 
 * @package Astre\Controller\InterfacageController
 */
class InterfacageController
{

    /** @var boolean|object */
    public $default = false;

    /**
     * InterfacageController constructor.
     */
    public function __construct()
    {
        global $sugar_config;

        $date = new DateTime('NOW');
        $organisme = (isset($sugar_config['opensub']['astre']['organisme'])) ? $sugar_config['opensub']['astre']['organisme'] : '';
        $code_budget = (isset($sugar_config['opensub']['astre']['code_budget'])) ? $sugar_config['opensub']['astre']['code_budget'] : '';
        $niv_trait = (isset($sugar_config['opensub']['astre']['niv_trait'])) ? $sugar_config['opensub']['astre']['niv_trait'] : '';
        $code_module = (isset($sugar_config['opensub']['astre']['code_module'])) ? $sugar_config['opensub']['astre']['code_module'] : '';
        $type_mouvement = (isset($sugar_config['opensub']['astre']['type_mouvement'])) ? $sugar_config['opensub']['astre']['type_mouvement'] : '';
        $type_liquidation = (isset($sugar_config['opensub']['astre']['type_liquidation'])) ? $sugar_config['opensub']['astre']['type_liquidation'] : '';
        $type_edit = (isset($sugar_config['opensub']['astre']['type_edit'])) ? $sugar_config['opensub']['astre']['type_edit'] : '';
        $ind_maj = (isset($sugar_config['opensub']['astre']['ind_maj'])) ? $sugar_config['opensub']['astre']['ind_maj'] : '';

        $this->default = (object) [
            'organisme' => $organisme,
            'annee' => $date->format('Y'),
            'date_jour' => str_replace('/', '', $date->format('d/m/Y')),
            'code_budget' => $code_budget,
            'niv_trait' => $niv_trait,
            'code_module' => $code_module,
            'type_mouvement' => $type_mouvement,
            'type_liquidation' => $type_liquidation,
            'type_edit' => $type_edit,
            'ind_maj' => $ind_maj,
        ];
    }

    /**
     * Fonction qui retourne le contenu du fichier d'interface
     * @param array $engagements
     * @return object
     */
    public function getFileContent($engagements = [])
    {
        $file_content = '';
        if (is_array($engagements) && count($engagements) > 0) {
            foreach ($engagements as $engagement) {
                $ligne = $this->getLigne($engagement['dossier_id'], $engagement['engagement_id'], $engagement['montant_a_liquider']);
                if ($ligne->statut) {
                    $file_content .= $ligne->data;
                }
            }
        }
        return (!empty($file_content)) ? (object) ['statut' => true, 'data' => $file_content] :  (object) ['statut' => false, 'data' => ''];
    }

    /**
     * Fonction qui retourne ligne de liquidation
     * @param string $engagement_id
     * @param string $dossier_id
     * @param float $montant_a_liquider
     * 
     * @return object
     */
    public function getLigne($dossier_id = '', $engagement_id = '', $montant_a_liquider = 0)
    {
        do {

            // On récupére l'objet OPS_engagement
            $obj_engagement = BeanFactory::getBean('OPS_engagement', $engagement_id);

            if (empty($obj_engagement->name) || empty($obj_engagement->ligne_credit)) break;
            if ($montant_a_liquider <= 0) break;

            // On récupére l'objet OPS_dossier
            $obj_dossier = BeanFactory::getBean('OPS_dossier', $dossier_id);

            if (empty($obj_dossier->num_dossier)) break;

            // On intialise les données de la liquidation
            $liquidation = (object) [
                'objet' => 'PST2 - Paiement Dossier ' . $obj_dossier->num_dossier,
                'dossier_num' => $obj_dossier->num_dossier,
                'montant_ttc' => InterfacageController::encodeMontant($montant_a_liquider),
                'date_liquidation' => $this->default->date_jour,
                'engagement_num' => $obj_engagement->name,
                'engagement_ligne' => $obj_engagement->ligne_credit,
            ];

            // On encode l'id du dossier et on le passe dans la variable Lib_comp1_ldc
            $dossier_id_encoded = InterfacageController::encodeModuleId($obj_dossier->id);
            
            // type financement 
            $obj_type_financement = str_replace( "^" , "", $obj_dossier->type_financement ) ;
            $type_financement = ( $obj_type_financement == '1' || $obj_type_financement == '2' ) ? "SUBIN" : "SUBFO";  


            // On génere la ligne de liquidation
            $interface = new InterfacageLiquidation([
                'Cod_coll' => $this->default->organisme, //   Organisme  *
                'Num_exbudg' => $this->default->annee, // Exercice budgétaire *
                'Cod_budg' => $this->default->code_budget, // Code budgétaire *
                'Ind_niv_trait' => $this->default->niv_trait, // Niveau de trait. 0,1,2,3 *
                'Cod_module' => $this->default->code_module, // Module de génération *
                'Typ_mvt_liq' => $this->default->type_mouvement, // Type de mouvement (C,A) *
                'Obj_ldc' => $liquidation->objet, // Objet de la liquidation *
                'Ref_int_ldc' => $liquidation->dossier_num, // Référence interne 
                'Mnt_ttc_ldc' => $liquidation->montant_ttc, // Montant ttc *
                'Typ_liq' => $this->default->type_liquidation, // Type liquidat. (I,C,M,R) *
                'Dat_ech_ldc' => $liquidation->date_liquidation, // Date d’échéance (JJMMYYYY) *
                'Typ_edit' => $this->default->type_edit, // Type d’édition demandée *
                'Num_eng' => $liquidation->engagement_num, // Numéro d’engagement
                'Num_lig_eng' => $liquidation->engagement_ligne, // Numéro ligne engagement
                'Ind_maj' => $this->default->ind_maj, // Autorisation maj gf *     
                'Lib_comp1_ldc' => $dossier_id_encoded, // Libellé complémentaire 1 
                'Idf_mdt' => $type_financement, // Libellé complémentaire 1 
            ]);
           
            return $interface->getLigne();
        } while (0);

        return (object) ['statut' => false, 'data' => '', 'erreurs' => []];
    }

    /**
     * Fonction qui encode l'id d'un module au format Astre GF
     * @param string $module_id
     * @return string
     */
    public static function encodeModuleId($module_id = '')
    {
        return strtoupper(str_replace('-', '', trim($module_id)));
    }

    /**
     * Fonction qui decode l'id d'un module au format UUid
     * @param string $module_id_encoded
     * @return string
     */
    public static function decodeModuleId($module_id_encoded = '')
    {
        if (empty($module_id_encoded) || strlen(trim($module_id_encoded)) !== 32) {
            return $module_id_encoded;
        }
        $module_id_encoded = strtolower($module_id_encoded);
        return substr($module_id_encoded, 0, 8) . '-' . substr($module_id_encoded, 8, 4) . '-' . substr($module_id_encoded, 12, 4) . '-' . substr($module_id_encoded, 16, 4) . '-' . substr($module_id_encoded, 20, 12);
    }

    /**
     * Fonction qui encode un montant format Astre
     * @param string $montant  120,15 => 12015
     * @return string
     */
    public static function encodeMontant($montant)
    {
        $montant = !empty($montant) ? floatval(str_replace(',', '.', $montant)) : 0;
        return $montant > 0 ? $montant * 100 : 0;
    }

    /**
     * Fonction qui decode un montant à partir du format Astre
     * @param string $montant 12015 => 120,15
     * @return string
     */
    public static function decodeMontant($montant)
    {
        return !empty($montant) && floatval($montant) > 0 ? floatval($montant) / 100 : 0;
    }

    /**
     * Fonction qui decode le montant depuis l'export BO
     * Je supprime les espaces floatval('12 000') === 12 et non 12000.00  
     * 
     * @param string $montant
     * @return string
     */

    public static function getFloat($montant = '')
    {
        return !empty($montant) ? floatval(str_replace(' ', '', str_replace(',', '.', $montant))) : 0;
    }

    /**
     * Fonction qui decode une date à partir du format Astre ( dd/mm/yy to dd/mm/yyyy )  
     * @param string $montant
     * @return string
     */
    public static function decodeDate($date = '')
    {
        $date_decoded = "";
        if (!empty($date)) {
            $date_array = array_filter(explode("/", $date), function ($v) {
                return !empty($v);
            }, ARRAY_FILTER_USE_BOTH);
            if (count($date_array) === 3) {
                $date_decoded = trim($date_array[0]) . '/' . trim($date_array[1]) . '/20' . trim($date_array[2]);
            }
        }
        return $date_decoded;
    }
}
