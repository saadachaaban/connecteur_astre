<?php

namespace Astre\Controller;

use BeanFactory;
use ODE\Service\DossierService;
use ODE\Service\EngagementService;
use ODE\Service\StatutService;
use ODE\Helper\OdeDateHelper;
use Astre\Controller\InterfacageController;

/**
 * Class FileImportController
 *
 * @package Astre\Controller
 */
class FileImportController
{

    /** @var string */
    public $file_path = '';

    /** @var string */
    public $statut_liquidation = '';

    /** @var string */
    public $statut_mandatement = '';

    /** @var string */
    public $statut_paiement = '';

    /**
     * FileImportController constructor.
     */
    public function __construct()
    {
        global $sugar_config;
        $this->file_path = (!empty($sugar_config['opensub']['astre']['path_file_update'])) ? $sugar_config['opensub']['astre']['path_file_update'] : '';
        $this->statut_liquidation = (!empty($sugar_config['opensub']['astre']['statut_liquidation'])) ? $sugar_config['opensub']['astre']['statut_liquidation'] : '';
        $this->statut_mandatement = (!empty($sugar_config['opensub']['astre']['statut_mandatement'])) ? $sugar_config['opensub']['astre']['statut_mandatement'] : '';
        $this->statut_paiement = (!empty($sugar_config['opensub']['astre']['statut_paiement'])) ? $sugar_config['opensub']['astre']['statut_paiement'] : '';
    }

    /**
     * Fonction qui met à jour les engagements et les liquidations
     * @param string $file_base64
     * @return object
     */
    public function updateInformationAstre($file_base64 = '')
    {

        // Mapping du fichier de création des liquidations 
        $mapping = ['dossier_id', 'engagement_num', 'liquidation_num', 'liquidation_date', 'liquidation_montant', 'mandat_num', 'mandat_date', 'mandat_montant', 'paiement_num', 'paiement_date', 'paiement_montant'];

        // On récupére les données depuis le fichier 
        $data = $this->getDataFromFile($file_base64, $mapping);

        // On boucle sur chaque ligne pour mettre à jour les engagements et créer les liquidations
        if (is_array($data) && count($data) > 0) {

            foreach ($data as $ligne) {

                do {

                    // On initialise un tableau détail journal 
                    $journal_details = [];
                    $statuts = [];
                    $obj_engagement = false;
                    $obj_liquidation = false;
                    $creation_liquidation = false;

                    // On vérifie les valeurs indispensables pour mettre à jour OpenSub
                    if (empty($ligne->dossier_id) || empty($ligne->engagement_num) || empty($ligne->liquidation_num)) break;

                    // On décode les dates
                    $ligne->liquidation_date = InterfacageController::decodeDate($ligne->liquidation_date);
                    $ligne->mandat_date = InterfacageController::decodeDate($ligne->mandat_date);
                    $ligne->paiement_date = InterfacageController::decodeDate($ligne->paiement_date);

                    // On décode l'id du dossier 
                    $dossier_id = InterfacageController::decodeModuleId($ligne->dossier_id);

                    // On récupére le dossier 
                    $obj_dossier = BeanFactory::getBean('OPS_dossier', $dossier_id);
                    if (empty($obj_dossier->id)) {
                        $GLOBALS['log']->fatal("FileImportController::updateDossier() => Le dossier ( id =" . $dossier_id . " ) n'a pas pu etre récupéré, la liquidation ( numéro = " . $ligne->liquidation_num . " ) n'a pas été créee ");
                        break;
                    }

                    // On récupére les engagements du dossier
                    $engagements = DossierService::getEngagements($dossier_id);
                    if (!is_array($engagements) || count($engagements) === 0) {
                        $GLOBALS['log']->fatal("FileImportController::updateDossier() => Le dossier ( id =" . $dossier_id . " ) ne dispose pas d'engagements, la liquidation ( numéro = " . $ligne->liquidation_num . " ) n'a pas été créee ");
                        break;
                    }

                    // On vérifie si l'engagement existe parmis les engagements du dossier [!! Si engagement en doublon il prendra 1 des deux xD !!]
                    foreach ($engagements as $engagement) {
                        if ($engagement->name === $ligne->engagement_num) {
                            $obj_engagement = BeanFactory::getBean('OPS_engagement', $engagement->id);
                        }
                    }
                    if (empty($obj_engagement->id)) {
                        $GLOBALS['log']->fatal("FileImportController::updateDossier() => L'engagement ( numéro =" . $ligne->engagement_num . " ) n'a pas pu etre récupéré, la liquidation ( numéro = " . $ligne->liquidation_num . " ) n'a pas été créee ");
                        break;
                    }

                    // On récupére les liquidations de l'engagement 
                    $liquidations = EngagementService::getLiquidations($obj_engagement->id);

                    // On vérifie si la liquidation existe => Ici aussi je ne controle pas l'unicité
                    foreach ($liquidations as $liquidation) {
                        if ($liquidation->name === $ligne->liquidation_num) {
                            $obj_liquidation = BeanFactory::getBean('OPS_liquidation', $liquidation->id);
                        }
                    }

                    // Si elle existe on vérifie si une valeur à changer 
                    if (!empty($obj_liquidation->id)) {


                        // ETAPE 1 : On vérifie les informations de la liquidation ( liquidation_date et liquidation_montant )
                        if (OdeDateHelper::toSQL($obj_liquidation->liquidation_date) !== OdeDateHelper::toSQL($ligne->liquidation_date)) {

                            // On initialise la ligne du journal
                            $detail = $this->getJournalDetail(
                                'Mise à jour de la liquidation N°' . $obj_liquidation->name,
                                'La valeur du champ liquidation_date a changée',
                                $obj_liquidation->liquidation_date,
                                $ligne->liquidation_date,
                                ''
                            );

                            // On met à jour la liquidation_date de la liquidation
                            $obj_liquidation->liquidation_date = $ligne->liquidation_date;

                            // La date de liquidation a changé on passe au statut liquidation
                            $statuts[] = $this->statut_liquidation;

                            $journal_details[] = $detail;
                        }
                        if (floatval($obj_liquidation->montant_ttc) !== InterfacageController::getFloat($ligne->liquidation_montant)) {

                            // On initialise la ligne du journal
                            $detail = $this->getJournalDetail(
                                'Mise à jour de la liquidation N°' . $obj_liquidation->name,
                                'La valeur du champ montant_ttc a changée',
                                floatval($obj_liquidation->montant_ttc),
                                InterfacageController::getFloat($ligne->liquidation_montant),
                                ''
                            );

                            // On modifie l'engagement pour mettre à jour le montant liquidé
                            $montant_total_liquide = floatval($obj_engagement->montant_liquide) - floatval($obj_liquidation->montant_ttc) + InterfacageController::getFloat($ligne->liquidation_montant);
                            $detail = $this->getJournalDetail(
                                'Mise à jour de l\'engagement N°' . $obj_engagement->name,
                                'La valeur du champ montant_liquide a changée',
                                $obj_engagement->montant_liquide,
                                $montant_total_liquide,
                                ''
                            );
                            $obj_engagement->montant_liquide = $montant_total_liquide;

                            $detail = $this->getJournalDetail(
                                'Mise à jour de l\'engagement N°' . $obj_engagement->name,
                                'La valeur du champ montant_restant_du a changée',
                                $obj_engagement->montant_restant_du,
                                $obj_engagement->montant_ttc - $montant_total_liquide,
                                ''
                            );
     			    $obj_engagement->montant_restant_du = $obj_engagement->montant_ttc - $montant_total_liquide;

                            // On met à jour la date_paiement de la liquidation
                            $obj_liquidation->montant_ttc = InterfacageController::getFloat($ligne->liquidation_montant);


                            $journal_details[] = $detail;
                        }


                        // Etape 2 : On vérifie les informations de mandatement ( mandat_num , mandat_date et mandat_montant )
                        if ($obj_liquidation->mandat_num !== $ligne->mandat_num) {

                            // On initialise la ligne du journal
                            $detail = $this->getJournalDetail(
                                'Mise à jour de la liquidation N°' . $obj_liquidation->name,
                                'La valeur du champ mandat_num a changée',
                                $obj_liquidation->mandat_num,
                                $ligne->mandat_num,
                                ''
                            );

                            // On met à jour la mandat_num de la liquidation
                            $obj_liquidation->mandat_num = $ligne->mandat_num;
                            $journal_details[] = $detail;
                        }
                        if (OdeDateHelper::toSQL($obj_liquidation->mandat_date) !== OdeDateHelper::toSQL($ligne->mandat_date)) {

                            // On initialise la ligne du journal
                            $detail = $this->getJournalDetail(
                                'Mise à jour de la liquidation N°' . $obj_liquidation->name,
                                'La valeur du champ mandat_date a changée',
                                $obj_liquidation->mandat_date,
                                $ligne->mandat_date,
                                ''
                            );

                            // On met à jour la mandat_date de la liquidation
                            $obj_liquidation->mandat_date = $ligne->mandat_date;

                            // La date du mandat a changé on passe au statut mandatement
                            $statuts[] = $this->statut_mandatement;

                            $journal_details[] = $detail;
                        }
                        if (floatval($obj_liquidation->mandat_montant) !== InterfacageController::getFloat($ligne->mandat_montant)) {

                            // On initialise la ligne du journal
                            $detail = $this->getJournalDetail(
                                'Mise à jour de la liquidation N°' . $obj_liquidation->name,
                                'La valeur du champ mandat_montant a changée',
                                $obj_liquidation->mandat_montant,
                                InterfacageController::getFloat($ligne->mandat_montant),
                                ''
                            );

                            // On modifie l'engagement pour mettre à jour le montant mandaté
                            $montant_total_mandate = floatval($obj_engagement->montant_mandate) - floatval($obj_liquidation->mandat_montant) + InterfacageController::getFloat($ligne->mandat_montant);
                            $detail = $this->getJournalDetail(
                                'Mise à jour de l\'engagement N°' . $obj_engagement->name,
                                'La valeur du champ montant_mandate a changée',
                                $obj_engagement->montant_mandate,
                                $montant_total_mandate,
                                ''
                            );
                            $obj_engagement->montant_mandate = $montant_total_mandate;

                            // On met à jour la date_paiement de la liquidation
                            $obj_liquidation->mandat_montant = InterfacageController::getFloat($ligne->mandat_montant);


                            $journal_details[] = $detail;
                        }

                        // Etape 3 : On vérifie les informations de paiement ( paiement_num , paiement_date et paiement_montant )
                        if ($obj_liquidation->paiement_num !== $ligne->paiement_num) {

                            // On initialise la ligne du journal
                            $detail = $this->getJournalDetail(
                                'Mise à jour de la liquidation N°' . $obj_liquidation->name,
                                'La valeur du champ paiement_num a changée',
                                $obj_liquidation->paiement_num,
                                $ligne->paiement_num,
                                ''
                            );

                            // On met à jour la paiement_num de la liquidation
                            $obj_liquidation->paiement_num = $ligne->paiement_num;
                            $journal_details[] = $detail;
                        }
                        if (OdeDateHelper::toSQL($obj_liquidation->paiement_date) !== OdeDateHelper::toSQL($ligne->paiement_date)) {

                            // On initialise la ligne du journal
                            $detail = $this->getJournalDetail(
                                'Mise à jour de la liquidation N°' . $obj_liquidation->name,
                                'La valeur du champ paiement_date a changée',
                                $obj_liquidation->paiement_date,
                                $ligne->paiement_date,
                                ''
                            );

                            // On met à jour la paiement_date de la liquidation
                            $obj_liquidation->paiement_date = $ligne->paiement_date;

                            // La date du mandat a changé on passe au statut paiement
                            $statuts[] = $this->statut_paiement;

                            $journal_details[] = $detail;
                        }
                        if (floatval($obj_liquidation->paiement_montant) !== InterfacageController::getFloat($ligne->paiement_montant)) {

                            // On initialise la ligne du journal
                            $detail = $this->getJournalDetail(
                                'Mise à jour de la liquidation N°' . $obj_liquidation->name,
                                'La valeur du champ paiement_montant a changée',
                                $obj_liquidation->paiement_montant,
                                InterfacageController::getFloat($ligne->paiement_montant),
                                ''
                            );


                            // On modifie l'engagement pour mettre à jour le montant mandaté
                            $montant_total_paye = floatval($obj_engagement->montant_paye) - floatval($obj_liquidation->paiement_montant) + InterfacageController::getFloat($ligne->paiement_montant);
                            $detail = $this->getJournalDetail(
                                'Mise à jour de l\'engagement N°' . $obj_engagement->name,
                                'La valeur du champ montant_paye a changée',
                                $obj_engagement->montant_paye,
                                $montant_total_paye,
                                ''
                            );
                            $obj_engagement->montant_paye = $montant_total_paye;

                            // On met à jour la date_paiement de la liquidation
                            $obj_liquidation->paiement_montant = InterfacageController::getFloat($ligne->paiement_montant);

                            $journal_details[] = $detail;
                        }
                    } else {


                        // La liquidation n'existe pas, on la crée 
                        $description = 'Données de la liquidation : ';
                        $description .= " \n" . '➤ N° liquidation : ' . $ligne->liquidation_num;
                        $description .= " \n" . '➤ Date liquidation : ' . $ligne->liquidation_date;
                        $description .= " \n" . '➤ Montant liquidé : ' . InterfacageController::getFloat($ligne->liquidation_montant) . '€ - Valeur brut : ' . $ligne->liquidation_montant;
                        $description .= " \n" . '➤ N° mandatement : ' . $ligne->mandat_num;
                        $description .= " \n" . '➤ Date mandatement : ' . $ligne->mandat_date;
                        $description .= " \n" . '➤ Montant mandaté : ' . InterfacageController::getFloat($ligne->mandat_montant) . '€ - Valeur brut : ' . $ligne->mandat_montant;
                        $description .= " \n" . '➤ N° de bordereau : ' . $ligne->paiement_num;
                        $description .= " \n" . '➤ Date paiement : ' . $ligne->paiement_date;
                        $description .= " \n" . '➤ Montant payé : ' . InterfacageController::getFloat($ligne->paiement_montant) . '€ - Valeur brut : ' . $ligne->paiement_montant;
                        $detail = $this->getJournalDetail(
                            'Création de la liquidation N°' . $ligne->liquidation_num,
                            'Génération de la liquidation',
                            '',
                            '',
                            $description
                        );

                        // On formate le montant avant insertion en BDD
                        $obj_liquidation = BeanFactory::newBean('OPS_liquidation');
                        $obj_liquidation->name = $ligne->liquidation_num;
                        $obj_liquidation->liquidation_date = $ligne->liquidation_date;
                        $obj_liquidation->montant_ttc = InterfacageController::getFloat($ligne->liquidation_montant);
                        $obj_liquidation->mandat_num = $ligne->mandat_num;
                        $obj_liquidation->mandat_date = $ligne->mandat_date;
                        $obj_liquidation->mandat_montant = InterfacageController::getFloat($ligne->mandat_montant);
                        $obj_liquidation->paiement_num = $ligne->paiement_num;
                        $obj_liquidation->paiement_date = $ligne->paiement_date;
                        $obj_liquidation->paiement_montant = InterfacageController::getFloat($ligne->paiement_montant);
                        $creation_liquidation = true;

                        // On passe le statut du dossier à liquidé 
                        $statuts[] = $this->statut_liquidation;

                        // Si on dispose dés la création de la date de mandatement => on passe le statut à mandatement juste apres liquidation
                        if (!empty($ligne->mandat_date)) {
                            $statuts[] = $this->statut_mandatement;
                        }

                        // Si on dispose dés la création de la date de paiement => on passe le statut à paiement juste apres mandatement
                        if (!empty($ligne->paiement_date)) {
                            $statuts[] = $this->statut_paiement;
                        }

                        $journal_details[] = $detail;
                    }

                    // Si on dispose de détails dans le journal c'est que la liquidation a été modifiée ou créée
                    if (is_array($journal_details) && count($journal_details) > 0) {

                        // On sauvegarde la liquidation
                        $obj_liquidation->save();

                        // Si il s'agit d'une création on crée la relation
                        if ($creation_liquidation) {
                            // On ajoute le montant au total liquidé du dossier
                            $obj_engagement->montant_liquide = $obj_engagement->montant_liquide + $obj_liquidation->montant_ttc;
                            $obj_engagement->montant_restant_du = $obj_engagement->montant_ttc - $obj_engagement->montant_liquide;
                            $obj_engagement->montant_mandate = $obj_engagement->montant_mandate + $obj_liquidation->mandat_montant;
                            $obj_engagement->montant_paye = $obj_engagement->montant_paye + $obj_liquidation->paiement_montant;
                            $obj_engagement->load_relationship('ops_engagement_ops_liquidation');
                            $obj_engagement->ops_engagement_ops_liquidation->add($obj_liquidation);
                        }

                        // On sauvegarde l'engagement
                        $obj_engagement->save();

                        // On modifie les champs custom du dossier
                        $old_champs_custom = (isset($obj_dossier->champs_custom) && !empty($obj_dossier->champs_custom)) ? json_decode(base64_decode($obj_dossier->champs_custom), true) : array();
                        $new_champs_custom = [
                            'engagement_num' => $ligne->engagement_num,
                            'liquidation_num' => $ligne->liquidation_num,
                            'liquidation_date' => $ligne->liquidation_date,
                            'liquidation_montant' => InterfacageController::getFloat($ligne->liquidation_montant),
                            'mandat_num' => $ligne->mandat_num,
                            'mandat_date' => $ligne->mandat_date,
                            'mandat_montant' => InterfacageController::getFloat($ligne->mandat_montant),
                            'paiement_num' => $ligne->paiement_num,
                            'paiement_date' => $ligne->paiement_date,
                            'paiement_montant' => InterfacageController::getFloat($ligne->paiement_montant),
                        ];
                        $champs_custom = array_merge($old_champs_custom, $new_champs_custom);
                        foreach ($champs_custom as $key => $value) {
                            if (is_string($value)) {
                                $champs_custom[$key] = stripslashes($value);
                            }
                        }

                        $obj_dossier->champs_custom = base64_encode(json_encode($champs_custom));
                        $obj_dossier->save();

                        // Modifications du statut 
                        if (is_array($statuts) && count($statuts) > 0) {
                            foreach ($statuts as $nouveau_statut_id) {
                                // Si le dossier est deja au bon statut, on ne rechange pas le statut
                                if ($obj_dossier->ops_statut_id !== $nouveau_statut_id) {
                                    $ancien_statut_name = $this->getStatutName($obj_dossier->ops_statut_id);
                                    $nouveau_statut_name = $this->getStatutName($nouveau_statut_id);
                                    $description = '➤ Ancien statut : ' . $ancien_statut_name . ' ( ID = ' . $obj_dossier->ops_statut_id . ' )';
                                    $description .= " \n" . '➤ Nouveau statut : ' . $nouveau_statut_name . ' ( ID = ' . $nouveau_statut_id . ' )';
                                    $journal_details[] = $this->getJournalDetail(
                                        'Changement du statut de ' . $ancien_statut_name . ' à ' . $nouveau_statut_name,
                                        'La liquidation a été créée ou modifiée',
                                        '',
                                        '',
                                        $description
                                    );
                                    $obj_dossier->updateStatut($nouveau_statut_id);
                                }
                            }
                        }

                        // On sauvegarde les modification
                        $obj_dossier->save();

                        // On crée le journal sur le dossier 
                        $journal_title = empty($file_base64) ? 'Mise à jour des informations Astre par tâche planifiée' : 'Mise à jour des informations Astre manuellement';
                        $obj_journal = BeanFactory::newBean('OPS_journal');
                        $obj_journal->name = $journal_title;
                        $obj_journal->parent_type = 'OPS_dossier';
                        $obj_journal->parent_id = $obj_dossier->id;
                        $obj_journal->statut = 'ok';
                        $obj_journal->save();


                        $obj_journal->load_relationship('ops_journal_detail_ops_journal');

                        // On crée les détails
                        foreach ($journal_details as $key => $detail) {
                            $obj_detail = BeanFactory::newBean('OPS_journal_detail');
                            $obj_detail->name = $detail->name;
                            $obj_detail->tache = $detail->tache;
                            $obj_detail->reponse = $detail->reponse;
                            $obj_detail->description = $detail->description;
                            $obj_detail->statut = $detail->statut;
                            $obj_detail->erreur = $detail->erreur;
                            $obj_detail->ordre = $key + 1;
                            $obj_detail->save();

                            $obj_journal->ops_journal_detail_ops_journal->add($obj_detail);
                        }
                    }
                } while (0);
            }
        }
    }

    /**
     * Fonction retourne les données du fichier 
     * @param string    $file_base64
     * @param array    $mapping
     * @return array
     */
    public function getDataFromFile($file_base64 = '', $mapping = [])
    {
        $data = [];

        do {

            if (!empty($file_base64)) {
                $file_content = base64_decode($file_base64);
            } else if (!empty($this->file_path) && file_exists($this->file_path)) {
                $file_content = file_get_contents($this->file_path);
            }

            if (empty($file_content)) break;

            $lignes = array_filter(explode("\n", $file_content), function ($v, $k) {
                return !empty(trim($v)) && $k !== 0;
            }, ARRAY_FILTER_USE_BOTH);

            if (!is_array($lignes) || count($lignes) === 0) break;

            foreach ($lignes as $ligne) {
                $champ = [];
                $champs = array_filter(explode("\t", $ligne), function ($v) {
                    return !empty($v);
                }, ARRAY_FILTER_USE_BOTH);
                foreach ($mapping as $mapping_key => $mapping_value) {
                    $champ[$mapping_value] = array_key_exists($mapping_key, $champs) ? $champs[$mapping_key] : '';
                }
                $data[] = (object) $champ;
            }
        } while (0);

        return $data;
    }

    /**
     * Fonction qui retourne le detail du journal
     * @param string    $name
     * @param string    $tache
     * @param string    $ancienne_valeur
     * @param string    $nouvelle_valeur
     * @param string    $description
     * @return object
     */
    public function getJournalDetail($name = '', $tache = '', $ancienne_valeur = '', $nouvelle_valeur = '', $description = '')
    {
        if (empty($description)) {
            $description = " \n" . '➤ Ancienne valeur : ' . $ancienne_valeur;
            $description .= " \n" . '➤ Nouvelle valeur : ' . $nouvelle_valeur;
        }
        return (object) [
            'name' => $name,
            'tache' => $tache,
            'reponse' => '',
            'statut' => 'ok',
            'erreur' => '',
            'description' => $description,
        ];
    }

    /**
     * Fonction qui retourne le libellé du statut
     * @param string    $description
     * @return string
     */
    public function getStatutName($statut_id = '')
    {
        $statut = StatutService::get($statut_id);
        return $statut !== false ? $statut->name : '';
    }
}
