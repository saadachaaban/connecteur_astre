<?php

namespace Astre\Interfacage;

/**
 * Class InterfacageLiquidation
 * 
 * @package Astre\Interfacage\InterfacageLiquidation
 */
final class InterfacageLiquidation
{

    /** @var bool */
    private $statut = true;

    /** @var array */
    private $variables = [];

    /** @var array */
    private $erreurs = [];

    /** @var array|object */
    protected $Cod_coll    = ['description' => 'Organisme', 'obligatoire' => true, 'longueur' => 5, 'position' => '1-5', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_exbudg  = ['description' => 'Exercice budgétaire', 'obligatoire' => true, 'longueur' => 4, 'position' => '6-9', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Cod_budg    = ['description' => 'Code budgétaire', 'obligatoire' => true, 'longueur' => 2, 'position' => '10-11', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_ldc    = ['description' => 'Numéro de la liquidation', 'obligatoire' => false, 'longueur' => 6, 'position' => '12-17', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Num_lig_ldc    = ['description' => 'Numéro de ligne de Liqd', 'obligatoire' => false, 'longueur' => 3, 'position' => '18-20', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Ind_niv_trait    = ['description' => 'Niveau de trait. 0,1,2,3', 'obligatoire' => true, 'longueur' => 1, 'position' => '21', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Cod_module    = ['description' => 'Module de génération', 'obligatoire' => true, 'longueur' => 2, 'position' => '22-23', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Typ_mvt_liq    = ['description' => 'Type de mouvement (C,A)', 'obligatoire' => true, 'longueur' => 1, 'position' => '24', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Obj_ldc    = ['description' => 'Objet de la liquidation', 'obligatoire' => true, 'longueur' => 40, 'position' => '25-64', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Lib_comp1_ldc    = ['description' => 'Libellé complémentaire 1', 'obligatoire' => false, 'longueur' => 60, 'position' => '63-124', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Lib_comp2_ldc    = ['description' => 'Libellé complémentaire 2', 'obligatoire' => false, 'longueur' => 60, 'position' => '125-184', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Ref_int_ldc    = ['description' => 'Référence interne', 'obligatoire' => false, 'longueur' => 20, 'position' => '185-204', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Idf_mdt    = ['description' => 'Identifiant de mandatement', 'obligatoire' => false, 'longueur' => 5, 'position' => '205-209', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Mnt_ttc_ldc    = ['description' => 'Montant ttc', 'obligatoire' => true, 'longueur' => 15, 'position' => '210-224', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Mnt_tva_ldc    = ['description' => 'Montant tva(= TVA proratisée si recup de TVA)', 'obligatoire' => false, 'longueur' => 15, 'position' => '225-239', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Cod_tva_ldc    = ['description' => 'Code tva appliqué', 'obligatoire' => false, 'longueur' => 2, 'position' => '240-241', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Typ_dev_ldc    = ['description' => 'Type de devise appliqué', 'obligatoire' => false, 'longueur' => 2, 'position' => '242-243', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Typ_liq    = ['description' => 'Type liquidat. (I,C,M,R)', 'obligatoire' => true, 'longueur' => 1, 'position' => '244', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Typ_deg    = ['description' => 'Type de dégagement (P,S)', 'obligatoire' => false, 'longueur' => 1, 'position' => '245', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Typ_lda    = ['description' => 'Type d’annulation (A,R,O)', 'obligatoire' => false, 'longueur' => 1, 'position' => '246', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Dat_ech_ldc    = ['description' => 'Date d’échéance (JJMMYYYY)', 'obligatoire' => true, 'longueur' => 8, 'position' => '247-254', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Num_mdt_ldc    = ['description' => 'Numéro de mandat', 'obligatoire' => false, 'longueur' => 6, 'position' => '255-260', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Num_bj_ldc    = ['description' => 'Numéro de bordereau journal', 'obligatoire' => false, 'longueur' => 6, 'position' => '261-266', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Typ_edit    = ['description' => 'Type d’édition demandée', 'obligatoire' => true, 'longueur' => 1, 'position' => '267', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_eng    = ['description' => 'Numéro d’engagement', 'obligatoire' => false, 'longueur' => 7, 'position' => '268-274', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_lig_eng    = ['description' => 'Numéro ligne engagement', 'obligatoire' => false, 'longueur' => 2, 'position' => '275-276', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Num_env    = ['description' => 'Ligne de crédit', 'obligatoire' => false, 'longueur' => 6, 'position' => '277-282', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Num_chap    = ['description' => 'Fonction', 'obligatoire' => false, 'longueur' => 4, 'position' => '283-286', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_schap    = ['description' => 'Sous-fonction', 'obligatoire' => false, 'longueur' => 4, 'position' => '287-290', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_art    = ['description' => 'Nature', 'obligatoire' => false, 'longueur' => 10, 'position' => '291-300', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_prg    = ['description' => 'Programme', 'obligatoire' => false, 'longueur' => 10, 'position' => '301-310', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Typ_mvt    = ['description' => 'Type de mouvement (ligne de crédit)', 'obligatoire' => false, 'longueur' => 1, 'position' => '311', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Cod_nature    = ['description' => 'Code nature (ligne de crédit)', 'obligatoire' => false, 'longueur' => 1, 'position' => '312', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_tiers    = ['description' => 'Numéro de fournisseur', 'obligatoire' => false, 'longueur' => 6, 'position' => '313-318', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_marche    = ['description' => 'Numéro de marché', 'obligatoire' => false, 'longueur' => 6, 'position' => '319-324', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_avenant    = ['description' => 'Numéro d’avenant', 'obligatoire' => false, 'longueur' => 2, 'position' => '325-326', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Lib_rais1_four    = ['description' => 'Raison soc. Fournisseur 1', 'obligatoire' => false, 'longueur' => 28, 'position' => '327-354', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Lib_rais2_four    = ['description' => 'Raison soc. Fournisseur 2', 'obligatoire' => false, 'longueur' => 28, 'position' => '355-382', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Lib_rais3_four    = ['description' => 'Raison soc. Fournisseur 3', 'obligatoire' => false, 'longueur' => 28, 'position' => '383-410', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Lib_adrue_four    = ['description' => 'Adresse rue fournisseur', 'obligatoire' => false, 'longueur' => 32, 'position' => '411-442', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Lib_adcomp_four    = ['description' => 'Adresse comp fournisseur', 'obligatoire' => false, 'longueur' => 32, 'position' => '443-474', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Lib_advil_four    = ['description' => 'Adresse ville fournis', 'obligatoire' => false, 'longueur' => 32, 'position' => '475-506', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Lib_burdis_four    = ['description' => 'Adresse bureau distrib.', 'obligatoire' => false, 'longueur' => 26, 'position' => '507-532', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Cod_pos_four    = ['description' => 'Code postal fournisseur', 'obligatoire' => false, 'longueur' => 5, 'position' => '533-537', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Cod_pays_four    = ['description' => 'Pays fournisseur', 'obligatoire' => false, 'longueur' => 3, 'position' => '538-540', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_dom_tiers    = ['description' => 'Numéro règlement tiers', 'obligatoire' => false, 'longueur' => 2, 'position' => '541-542', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Cod_paie_ldc    = ['description' => 'Code de paiement', 'obligatoire' => false, 'longueur' => 3, 'position' => '543-545', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Cod_bang_ldc    = ['description' => 'Code banque', 'obligatoire' => false, 'longueur' => 5, 'position' => '546-550', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Cod_guic_ldc    = ['description' => 'Code guichet', 'obligatoire' => false, 'longueur' => 5, 'position' => '551-555', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_cpt_ldc    = ['description' => 'Numéro de compte', 'obligatoire' => false, 'longueur' => 11, 'position' => '556-566', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_rib_ldc    = ['description' => 'Numéro de cle RIB', 'obligatoire' => false, 'longueur' => 2, 'position' => '567-568', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Int_cpt_ldc    = ['description' => 'Intitulé du compte', 'obligatoire' => false, 'longueur' => 40, 'position' => '569-608', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Typ_vers    = ['description' => 'Type de versement (H,I,N)', 'obligatoire' => false, 'longueur' => 1, 'position' => '609', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Ind_maj    = ['description' => 'Autorisation maj gf', 'obligatoire' => true, 'longueur' => 1, 'position' => '610', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Ref_liaison    = ['description' => 'Références pour infos', 'obligatoire' => false, 'longueur' => 80, 'position' => '611-690', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_enr    = ['description' => 'Numéro d’enregistrement', 'obligatoire' => false, 'longueur' => 6, 'position' => '691-696', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Cod_nat_rct    = ['description' => 'Code nature fichier RCT', 'obligatoire' => false, 'longueur' => 2, 'position' => '697-698', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Num_engap    = ['description' => 'Numéro d’engagement AP', 'obligatoire' => false, 'longueur' => 6, 'position' => '699-704', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Mil_engap    = ['description' => 'Millésime engagement AP', 'obligatoire' => false, 'longueur' => 4, 'position' => '705-708', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Idoperat    = ['description' => 'Numéro d’opération', 'obligatoire' => false, 'longueur' => 8, 'position' => '709-716', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Idphase    = ['description' => 'Numéro de l’opération nature', 'obligatoire' => false, 'longueur' => 8, 'position' => '717-724', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_lot    = ['description' => 'Numéro de lot d’1 marché', 'obligatoire' => false, 'longueur' => 5, 'position' => '725-729', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Crit_vnt1    = ['description' => 'Critère de ventilation 1', 'obligatoire' => false, 'longueur' => 5, 'position' => '730-734', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Crit_vnt2    = ['description' => 'Critère de ventilation 2', 'obligatoire' => false, 'longueur' => 5, 'position' => '735-739', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Crit_vnt3    = ['description' => 'Critère de ventilation 3', 'obligatoire' => false, 'longueur' => 5, 'position' => '740-744', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Crit_vnt4    = ['description' => 'Critère de ventilation 4', 'obligatoire' => false, 'longueur' => 5, 'position' => '745-749', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Crit_vnt5    = ['description' => 'Critère de ventilation 5', 'obligatoire' => false, 'longueur' => 5, 'position' => '750-754', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_invent    = ['description' => 'Numéro inventaire INDIGO', 'obligatoire' => false, 'longueur' => 25, 'position' => '755-779', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Cpt_rcttie    = ['description' => 'Compte de tiers pour RCT', 'obligatoire' => false, 'longueur' => 5, 'position' => '780-784', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_tranche    = ['description' => 'Numéro tranche du marché (pas utilisé)', 'obligatoire' => false, 'longueur' => 1, 'position' => '785-785', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Mil_mar    = ['description' => 'Millésime du marché', 'obligatoire' => false, 'longueur' => 4, 'position' => '786-789', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_cpp    = ['description' => 'Numéro de cpp de la liqd', 'obligatoire' => false, 'longueur' => 5, 'position' => '790-794', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Monnaie_ent    = ['description' => 'Monnaie d’entrée', 'obligatoire' => false, 'longueur' => 1, 'position' => '795', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Monnaie_sor    = ['description' => 'Monnaie de sortie', 'obligatoire' => false, 'longueur' => 1, 'position' => '796', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_fac    = ['description' => 'Numéro de facture', 'obligatoire' => false, 'longueur' => 6, 'position' => '797-802', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Contexte    = ['description' => 'Contexte', 'obligatoire' => false, 'longueur' => 30, 'position' => '803-832', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Taux_fctva    = ['description' => 'Taux de fctva', 'obligatoire' => false, 'longueur' => 2, 'position' => '833-834', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Mnt_fctva    = ['description' => 'Montant de fctva', 'obligatoire' => false, 'longueur' => 15, 'position' => '835-849', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Num_pjdo    = ['description' => 'Numéro de pièce jointe', 'obligatoire' => false, 'longueur' => 5, 'position' => '850-854', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Stat_liq    = ['description' => 'Statut de la liquidation', 'obligatoire' => false, 'longueur' => 1, 'position' => '855', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Idf_mdt_val    = ['description' => 'Identifiant validant', 'obligatoire' => false, 'longueur' => 5, 'position' => '856-860', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Typ_nomenc_mar    = ['description' => 'Type nomenclature marché', 'obligatoire' => false, 'longueur' => 2, 'position' => '861-862', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Cod_nomenc_mar    = ['description' => 'Code nomenclature marché', 'obligatoire' => false, 'longueur' => 10, 'position' => '863-872', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Mnt_ht_ldc    = ['description' => 'Montant HT (par défaut = TTCTVA)', 'obligatoire' => false, 'longueur' => 15, 'position' => '873-887', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Delai    = ['description' => 'Délai de paiement', 'obligatoire' => false, 'longueur' => 3, 'position' => '888-890', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Dat_dep_delai    = ['description' => 'Date de départ du délai', 'obligatoire' => false, 'longueur' => 7, 'position' => '891-897', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Cod_etci    = ['description' => 'Etat civil', 'obligatoire' => false, 'longueur' => 6, 'position' => '898-903', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Cod_debit    = ['description' => 'Code débiteur', 'obligatoire' => false, 'longueur' => 2, 'position' => '904-905', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Adr_id    = ['description' => 'Identifiant de l’adresse de paiement (par défaut = adresse de paiement du tiers)', 'obligatoire' => false, 'longueur' => 22, 'position' => '906-927', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Cod_bic    = ['description' => 'Code BIC', 'obligatoire' => false, 'longueur' => 11, 'position' => '928-938', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Lib_cpt_etr    = ['description' => 'Libellé 1 de la banque étrangère', 'obligatoire' => false, 'longueur' => 24, 'position' => '939-962', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Lib2_cpt_etr    = ['description' => 'Libellé 2 de la banque étrangère', 'obligatoire' => false, 'longueur' => 76, 'position' => '963-1038', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_cpt_etr    = ['description' => 'Numéro du compte étranger', 'obligatoire' => false, 'longueur' => 30, 'position' => '1039-1068', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Cod_iso2    = ['description' => 'Code ISO2 du pays du compte étranger', 'obligatoire' => false, 'longueur' => 2, 'position' => '1069-1070', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Cod_regie    = ['description' => 'Code régie', 'obligatoire' => false, 'longueur' => 5, 'position' => '1071-1075', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Typ_pce_pes    = ['description' => 'Type de pièce PES', 'obligatoire' => false, 'longueur' => 2, 'position' => '1076-1077', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Nat_pce_pes    = ['description' => 'Nature de la pièce PES', 'obligatoire' => false, 'longueur' => 2, 'position' => '1078-1079', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Filler    = ['description' => 'Filler', 'obligatoire' => false, 'longueur' => 58, 'position' => '1080-1137', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Tva_Intra_Comm    = ['description' => 'Tva intra-communautaire (0 ou 1)', 'obligatoire' => false, 'longueur' => 1, 'position' => '1138', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Serv_Indigo    = ['description' => 'Service protocole indigo', 'obligatoire' => false, 'longueur' => 10, 'position' => '1139-1148', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Cat_tiers    = ['description' => 'Catégorie de tiers', 'obligatoire' => false, 'longueur' => 2, 'position' => '1149-1150', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Prenom    = ['description' => 'Prénom du tiers', 'obligatoire' => false, 'longueur' => 38, 'position' => '1151-1188', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Idemprunt    = ['description' => 'Numéro d’emprunt ordonnateur', 'obligatoire' => false, 'longueur' => 24, 'position' => '1189-1212', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Pes_ExBudg_Ratt    = ['description' => 'Exercice budgétaire de rattachment', 'obligatoire' => false, 'longueur' => 4, 'position' => '1213-1216', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Pes_MdtTit_Ratt    = ['description' => 'Numéro du Mandat/Titre de rattachement', 'obligatoire' => false, 'longueur' => 12, 'position' => '1217–1228', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Pes_Lig_Ratt    = ['description' => 'Numéro de la ligne du Mandat/Titre de rattachement', 'obligatoire' => false, 'longueur' => 6, 'position' => '1229-1234', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Tva_recup_id    = ['description' => 'Numéro d’ordre du taux de tva récupérable (si récup de TVA)', 'obligatoire' => false, 'longueur' => 10, 'position' => '1235-1244', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Cod_etci_pes    = ['description' => 'Code Pes de la civilité', 'obligatoire' => false, 'longueur' => 10, 'position' => '1245', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Cle_iban    = ['description' => 'Cle iban', 'obligatoire' => false, 'longueur' => 2, 'position' => '1255', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Ind_sepa    = ['description' => 'Elligibilité sepa (0/1/null)', 'obligatoire' => false, 'longueur' => 1, 'position' => '1257', 'type' => 'integer', 'value' => ''];

    /** @var array|object */
    protected $Cle_liq_pj    = ['description' => 'Référence unique DA/PJ', 'obligatoire' => false, 'longueur' => 30, 'position' => '1258', 'type' => 'string', 'value' => ''];

    /** @var array  Date DDMMYYYY */
    protected $Dat_ech_pes    = ['description' => 'Date à juste paiement', 'obligatoire' => false, 'longueur' => 8, 'position' => '1288', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Num_tiers_benef    = ['description' => 'N° tiers beneficiaire', 'obligatoire' => false, 'longueur' => 6, 'position' => '1296', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Vd_num_fac    = ['description' => 'Numero de la facture interfacee', 'obligatoire' => false, 'longueur' => 15, 'position' => '1302', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Cod_budg_fac    = ['description' => 'Code budget de la facture interfacee', 'obligatoire' => false, 'longueur' => 2, 'position' => '1317', 'type' => 'string', 'value' => ''];

    /** @var array|object */
    protected $Comp_typ_vers    = ['description' => 'Type de versement sur 3 car(pour DSN)', 'obligatoire' => false, 'longueur' => 3, 'position' => '1319', 'type' => 'string', 'value' => ''];

    /**
     * InterfacageLiquidation constructor.
     * @param array $data : Les données d'initialisation
     */
    public function __construct($data = [])
    {

        // On récupére les variables de la classes sauf le statut, le tableau des variables et le tableau des erreurs
        $this->variables = array_filter(get_class_vars(get_class($this)), function ($k) {
            return $k !== 'statut' && $k !== 'variables' && $k !== 'erreurs';
        }, ARRAY_FILTER_USE_KEY);

        // J'ai tout mis en object et je me suis rendu compte que j'avais un tableau xD 
        foreach ($this->variables as $variable_name => $variable) {
            $this->variables[$variable_name] = $this->$variable_name = (object) $variable;
        }

        // On boucle sur les variables pour vérifier, formater leurs valeurs
        foreach ($this->variables as $variable_name => $variable) {

            // On récupére la valeur d'initialisation par type
            $valeur = $variable->type === 'string' ? $this->getValeurTypeString($data, $variable_name) : $this->getValeurTypeInteger($data, $variable_name);

            // Si la valeur est vide et obligatoire le statut passe à false et l'erreur est sauvegardée, je ne vérifie pas si c'est !empty() pour laisser passer les 0
            if ($variable->obligatoire === true && $valeur === "") {
                $this->statut = false;
                $this->erreurs[$variable_name] = '[' . $variable_name . '] ' . $variable->description . ' est obligatoire';
            } else {
                // Sinon on affecte la valeur
                $this->$variable_name->value = !empty($valeur) ? $valeur : '';
            }
        }
    }

    /**
     * Fonction qui retourne la ligne de liquidation
     * @return object
     */
    function getLigne()
    {
        if ($this->statut) {
            $ligne = '';
            foreach ($this->variables as $variable) {
                if (strlen($variable->value) === $variable->longueur) {
                    $ligne .= $variable->value;
                } else if (strlen($variable->value) > $variable->longueur) {
                    $ligne .= substr($variable->value, 0, $variable->longueur);
                } else {
                    $ligne .= $variable->value . str_repeat(' ', $variable->longueur - strlen($variable->value));
                }
            }
            $ligne .= "\n";
        }
        return  count($this->erreurs) === 0 ? (object) ['statut' => true, 'data' => $ligne, 'erreurs' => $this->erreurs] : ['statut' => false, 'data' => '', 'erreurs' => $this->erreurs];
    }

    /**
     * Fonction qui vérifie la valeur de variable de type String
     * @param array $data
     * @param string $variable_name
     * @return string
     */
    private function getValeurTypeString($data, $variable_name)
    {

        // On récupére la valeur d'initialisation
        $valeur = array_key_exists($variable_name, $data) && trim($data[$variable_name]) !== "" ? trim($data[$variable_name]) : '';

        // On remplace les accents
        $search  = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ');
        $replace = array('A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y');
        $valeur = str_replace($search, $replace, $valeur);

        // On supprime les caracteres spéciaux 
        $valeur = preg_replace("/[^a-zA-Z0-9  ]+/", '', $valeur); //  /[^a-zA-ZÀ-ÿ0-9  ]+/;

        // On retourne la valeur en majuscule
        return strtoupper($valeur);
    }

    /**
     * Fonction qui vérifie la valeur de variable de type Integer
     * @param array $data
     * @param string $variable_name
     * @return int
     */
    private function getValeurTypeInteger($data, $variable_name)
    {
        // On récupére la valeur d'initialisation
        $valeur = array_key_exists($variable_name, $data) && !empty(trim($data[$variable_name])) ? trim($data[$variable_name]) : '';
        return intval($valeur);
    }
}
