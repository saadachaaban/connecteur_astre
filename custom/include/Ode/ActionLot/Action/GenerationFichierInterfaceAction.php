<?php

namespace ODE\ActionLot\Action;

if (!defined('sugarEntry')) define('sugarEntry', true);

use ODE\Modal\OdeModal;

class GenerationFichierInterfaceAction
{

    /**
     * @access public
     * @name getAction()
     * Fonction qui retourne l'action "Générer le fichier d'interface"
     * 
     *  @return array       - $action
     */
    public function getAction()
    {
        return [
            'module_name' => 'OPS_dossier',
            'action_content' => $this->getActionContent(),
            'action_link' => $this->getActionLink(),
        ];
    }

    /**
     * @access public
     * @name getActionOrdre()
     * Fonction qui retourne l'ordre de l'action
     * 
     *  @return int       - $action_ordre
     */
    public function getActionOrdre()
    {
        return 6;
    }

    /**
     * @access private
     * @name getActionContent()
     * Fonction qui retourne le content de l'action
     * 
     *  @return string       - $action_content
     */
    private function getActionContent()
    {
        return $this->getActionLink() . $this->getActionHTML();
    }

    /**
     * @access private
     * @name getActionLink()
     * Fonction qui retourne le link de l'action
     * 
     *  @return string       - $action_link
     */
    private function getActionLink()
    {
        return '<a role="ode-modal-generation-interface-btn" > Générer le fichier d\'interface </a>';
    }

    /**
     * @access private
     * @name getActionHTML()
     * Fonction qui retourne l'html de l'action
     * 
     *  @return string       - $action_html
     */
    private function getActionHTML()
    {
        $modal = new OdeModal('ode_modal_generation_interface');
        $modal->setTitle('Générer le fichier d\'interface');
        $modal->setCss(['custom/modules/OPS_dossier/css/generation_interface.css']);
        $modal->setJS(['custom/modules/OPS_dossier/js/generation_interface.js']);
        $modal->setPages(['custom/modules/OPS_dossier/tpls/generation_interface.tpl']);
        return $modal->getHtml();
    }
}
