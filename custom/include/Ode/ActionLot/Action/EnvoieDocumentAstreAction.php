<?php

namespace ODE\ActionLot\Action;

if (!defined('sugarEntry')) define('sugarEntry', true);

use ODE\Modal\OdeModal;

class EnvoieDocumentAstreAction
{

    /**
     * @access public
     * @name getAction()
     * Fonction qui retourne l'action "Télécharger les documents Astre"
     * 
     *  @return array       - $action
     */
    public function getAction()
    {
        return [
            'module_name' => 'OPS_dossier',
            'action_content' => $this->getActionContent(),
            'action_link' => $this->getActionLink(),
        ];
    }

    /**
     * @access public
     * @name getActionOrdre()
     * Fonction qui retourne l'ordre de l'action
     * 
     *  @return int       - $action_ordre
     */
    public function getActionOrdre()
    {
        return 7;
    }

    /**
     * @access private
     * @name getActionContent()
     * Fonction qui retourne le content de l'action
     * 
     *  @return string       - $action_content
     */
    private function getActionContent()
    {
        return $this->getActionLink() . $this->getActionHTML();
    }

    /**
     * @access private
     * @name getActionLink()
     * Fonction qui retourne le link de l'action
     * 
     *  @return string       - $action_link
     */
    private function getActionLink()
    {
        return '<a role="ode-modal-envoie-document-btn" > Télécharger les documents Astre </a>';
    }

    /**
     * @access private
     * @name getActionHTML()
     * Fonction qui retourne l'html de l'action
     * 
     *  @return string       - $action_html
     */
    private function getActionHTML()
    {
        $modal = new OdeModal('ode_modal_envoie_document');
        $modal->setTitle('Télécharger les documents Astre');
        $modal->setCss(['custom/modules/OPS_dossier/css/envoie_document.css']);
        $modal->setJS(['custom/modules/OPS_dossier/js/envoie_document.js']);
        $modal->setPages(['custom/modules/OPS_dossier/tpls/envoie_document.tpl']);
        return $modal->getHtml();
    }
}
