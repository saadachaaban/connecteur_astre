<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<!-- Modal CSS -->
{$styles}
<!-- Modal CSS -->

<div id="{$modal_id}" class="modal" role="ode-modal">

    <!-- Modal Header --> 
    <div role="ode-modal-header" class="ode-modal-header" > 
        <h2> {$modal_title} </h2>
    </div>

    <div role="ode-modal-pages" class="ode-modal-pages"> 

        <!-- Loading Page -->  
        <div role="ode-modal-page" data-route="loading" class="ode-modal-page">
            <div role="ode-loading-pourcentage" class="ode-loading-pourcentage">0%</div>
            <div role="ode-loading-container" class="ode-loading-container">
                <div role="ode-loading-bar" class="ode-loading-bar" data-current='0' data-total='0' ></div>
            </div>
            <div role="ode-loading-message" class="ode-loading-message loading-dots" > Traitement en cours </div>
        </div>

        <!-- Erreur Page --> 
        <div role="ode-modal-page" data-route="erreur" class="ode-modal-page">
            <h3 role="message-erreur"></h3>
            <p> Code sortie : 400 </p>
            <img src="custom/include/Ode/Modal/img/erreur_technique.png">
        </div>

        <!-- Custom Pages --> 
        {$pages}

        <!-- Footer Page : buttons -->
        <div>
            <div role="ode-modal-information" class="ode-modal-information">
                <i class="fa-solid fa-circle-exclamation"></i>
                <h3></h3>
            </div>
            <div role="ode-modal-boutons" class="ode-modal-boutons"></div>
        </div>

    </div>

</div>

<!-- Modal JS -->
{$scripts}
<!-- Modal JS -->