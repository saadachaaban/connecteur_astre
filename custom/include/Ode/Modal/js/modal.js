var OdeModal = { loaded: false, modal_id: '', boutons: false, information: false, loading: { loaded: false } };

OdeModal.initialisation = function( modal_id = ''){ 

    if ( modal_id === '' || $(`#${modal_id}`).length === 0 ) {
        console.log("OdeModal::initialisation => l'élement id=" + modal_id + " n'existe pas");
        return;
    }

    var progress_bar = Object.create(OdeProgressBar);
    progress_bar.initialisation( modal_id );
    if ( progress_bar.loaded === false ) {
        console.log("OdeModal::initialisation => erreur d'initialisation de la page de loading de l'élement id=" + modal_id );
        return;
    }

    const bouton_container = $(`#${modal_id} [role="ode-modal-pages"] [role="ode-modal-boutons"]`);
    if ( bouton_container.length === 0 ) {
        console.log("OdeModal::initialisation => erreur d'initialisation des boutons de la modale id=" + modal_id );
        return;
    }

    this.boutons = (function(container) {
        return {
            add: function( libelle , attributs ) {
                var bouton = `<button type="button" class="btn btn-primary"`;
                $.each( attributs, function( attribut_name, attribut_value ) {
                    bouton += ` ${attribut_name}="${attribut_value}" `;
                });
                bouton += `> ${libelle} </button>`;
                container.append(bouton);
            },
            empty: function() { container.empty(); },
            show: function() { container.css('display', 'inline-block'); },
            hide: function() { container.hide(); },
        }
    })(bouton_container);

    const information_container = $(`#${modal_id} [role="ode-modal-pages"] [role="ode-modal-information"]`);
    if ( information_container.length === 0 ) {
        console.log("OdeModal::initialisation => erreur d'initialisation de la zone information de la modale id=" + modal_id );
        return;
    }

    this.information = (function(container) {
        return {
            add: function( information ) {
                information = ( information !== undefined && information !== '' ) ? information : '';
                container.find('h3').html(information);
            },
            show: function() { container.css('display', 'inline-block'); },
            hide: function() { container.hide(); },
        }
    })(information_container);

    this.loading = progress_bar;
    this.modal_id = modal_id;
    this.loaded = true;
    
}

OdeModal.getWidth = function(){ return window.innerWidth / 1.2; }

OdeModal.getHeight = function(){ return window.innerHeight / 1.4; }

OdeModal.resize = function(){ 
    $('[role="ode-modal"]').css("width", `${OdeModal.getWidth()}px`);
    $('[role="ode-modal"]').css("max-width", `${OdeModal.getWidth()}px`);
    $('[role="ode-modal"]').css("height", `${OdeModal.getHeight()}px`);
}

OdeModal.open = function(){ 

    if ( this.loaded ) {

        $(`#${this.modal_id}`).f_modal({ escapeClose: false, clickClose: false });
            var close_btn = $(`#${this.modal_id} [rel="modal:close"]`);
            close_btn.attr('rel','');
            close_btn.off().on('click', function () {
            $.f_modal.close();
        });

        this.loading.reset();
        this.information.hide();
        this.boutons.empty();
        this.boutons.hide();
        this.resize();

        $( window ).resize(function() { OdeModal.resize(); });

    }

}

OdeModal.getPages = function(){ return $(`#${this.modal_id} [role="ode-modal-page"]`); }

OdeModal.isPageExist = function( route ){ 
    return ( $(`#${this.modal_id} [role="ode-modal-page"][data-route="${route}"]`).length > 0 ) ? true : false;
}

OdeModal.setErreurMessage = function( message = '' ){ 

    message = ( message !== undefined && message !== '' ) ? message : 'Erreur technique, veuillez contacter votre administrateur';

    if ( $(`#${this.modal_id} [role="ode-modal-page"][data-route="erreur"] [role="message-erreur"]`).length > 0 ){
        $(`#${this.modal_id} [role="ode-modal-page"][data-route="erreur"] [role="message-erreur"]`).html(message);
    }

}

OdeModal.route = function( route, message = ''){ 
    
    if ( this.loaded ) {

        route = ( this.isPageExist( route ) ) ? route : 'erreur';

        this.setErreurMessage( message );

        const pages = this.getPages();
        pages.each(function() {
            ( $(this).attr('data-route') === route ) ? $(this).show() : $(this).hide();
        });

    }

}

OdeModal.getSelectedDossiers = function(){
    var tabId = new Array();
    for (var wp = 0; wp < document.MassUpdate.elements.length; wp++) {
        if (document.MassUpdate.elements[wp].name == 'current_query_by_page') {
            json = document.MassUpdate.elements[wp].value;
        }
        var reg_for_existing_uid = new RegExp('^' + RegExp.escape(document.MassUpdate.elements[wp].value) + '[\s]*,|,[\s]*' + RegExp.escape(document.MassUpdate.elements[wp].value) + '[\s]*,|,[\s]*' + RegExp.escape(document.MassUpdate.elements[wp].value) + '$|^' + RegExp.escape(document.MassUpdate.elements[wp].value) + '$');
        if (typeof document.MassUpdate.elements[wp].name != 'undefined' &&
            document.MassUpdate.elements[wp].name == 'mass[]' &&
            document.MassUpdate.elements[wp].checked &&
            !reg_for_existing_uid.test(document.MassUpdate.uid.value)) {
            tabId.push(document.MassUpdate.elements[wp].value);
        }
    }
    return ( document.MassUpdate.select_entire_list.value == 1 ) ? { dossiers: false, filtres: json } : { dossiers: tabId, filtres: false };
} 

OdeModal.sleep = function(ms) { return new Promise(resolve => setTimeout(resolve, ms)); }

OdeModal.initDataTable = function( route ){ 
    const table = $(`#${this.modal_id} [role="ode-modal-page"][data-route="${route}"] table`);
    const initialisation = ( $(`#${this.modal_id} [role="ode-modal-page"][data-route="${route}"] .dataTables_wrapper`).length > 0 ) ? true : false;
    // $.fn.dataTable.isDataTable(table) retourne false dans les deux cas du coup je test l'existance du wrapper
    if ( table.length > 0 && initialisation === false) {
        table.DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.11.5/i18n/fr-FR.json"
            }
        });
        this.resize();
    }
}


var OdeProgressBar = {         
    bar: false,
    bar_message: false,
    bar_pourcentage: false,
    current: 0,
    total: 0,
    pourcentage: 0,
    message: '',
    message_defaut: 'Traitement en cours',
    loaded: false
};

OdeProgressBar.initialisation = function( modal_id = '' ){

    if ( $(`#${modal_id} [role="ode-modal-page"][data-route="loading"] [role="ode-loading-bar"]`).length === 0 ) {
        console.log('OdeProgressBar => Erreur de récupération de ode-loading-bar');
        return;
    }

    if ( $(`#${modal_id} [role="ode-modal-page"][data-route="loading"] [role="ode-loading-pourcentage"]`).length === 0 ) {
        console.log('OdeProgressBar => Erreur de récupération de ode-loading-pourcentage');
        return;
    }

    if ( $(`#${modal_id} [role="ode-modal-page"][data-route="loading"] [role="ode-loading-message"]`).length === 0 ) {
        console.log('OdeProgressBar => Erreur de récupération de ode-loading-message');
        return;
    }
    
    this.bar = $(`#${modal_id} [role="ode-modal-page"][data-route="loading"] [role="ode-loading-bar"]`);
    this.bar_message = $(`#${modal_id} [role="ode-modal-page"][data-route="loading"] [role="ode-loading-message"]`);
    this.bar_pourcentage = $(`#${modal_id} [role="ode-modal-page"][data-route="loading"] [role="ode-loading-pourcentage"]`);

    this.reset();

    this.loaded = true;
}

OdeProgressBar.reset = function(){ 
    if ( this.loaded ) {
        this.setTotal(0);
        this.bar.css('transition-duration','0s');
        this.setCurrent(0);
        this.bar.css('transition-duration','1s');
        this.setMessage('');
    }
}

OdeProgressBar.setTotal = function( total = 0 ){ 
    this.total = total;
    this.bar.attr('data-total', total);
}

OdeProgressBar.setCurrent = function( current = 0 ){ 
    this.current = current;
    this.bar.attr('data-current', current);
    this.setPourcentage();
}

OdeProgressBar.setPourcentage = function(){ 
    this.pourcentage = this.getPourcentage();
    this.bar_pourcentage.html(`${this.pourcentage}%`);
    this.bar.css("width",`${this.pourcentage}%`);
}

OdeProgressBar.setMessage = function( message = '' ){ 
    this.message =  ( message !== '') ? message : this.message_defaut;
    this.bar_message.html( this.message );
}

OdeProgressBar.getPourcentage = function(){ 
    if ( this.total === 0 ) return 0;
    if ( this.total === this.current || this.current > this.total) return 100;
    return Math.floor( ( this.current * 100 ) / this.total );
}

OdeProgressBar.increaseCurrent = function(){ 
    var current = parseInt(this.current) + 1;            
    this.setCurrent( current ); 
}

