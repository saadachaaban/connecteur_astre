<?php

namespace ODE\Service;

use DBManagerFactory;

/**
 * Class StatutService
 *
 * @package ODE\Service
 */
class StatutService
{

    /**
     * Fonction qui retourne l'object statut
     * @param string   $statut_id
     * @return false|object
     */
    static function get($statut_id = '')
    {
        $db = DBManagerFactory::getInstance();
        if (!empty($statut_id)) {
            $sql = "SELECT * FROM `ops_statut` WHERE `id` = '" . $statut_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $statut = $row;
            }
            if (is_array($statut) && count($statut) > 0) {
                $statut['module_type'] = 'OPS_statut';
                $statut['name_display'] = $statut['name'];
            }
        } else {
            $GLOBALS['log']->fatal("StatutService::get() => L'id du statut à récupérer est vide");
        }
        return (is_array($statut) && count($statut) > 0) ? (object) $statut : false;
    }

    /**
     * Fonction qui retourne le libellé du guide d'instruction du statut
     * @param string   $statut_id
     * @return false|object
     */
    public static function getGuideInstructionName($statut_id = '')
    {
        $db = DBManagerFactory::getInstance();
        $guides = array();
        if (!empty($statut_id)) {
            $sql = "SELECT `name`
            FROM `ops_guide_instruction`
            INNER JOIN `ops_guide_instruction_ops_etape` ON `ops_guide_instruction`.id = `ops_guide_instruction_ops_etape`.ops_guide_instruction_id
            INNER JOIN `ops_etape_ops_statut` ON `ops_guide_instruction_ops_etape`.ops_etape_id = `ops_etape_ops_statut`.ops_etape_id
            WHERE `ops_etape_ops_statut`.ops_statut_id ='" . $statut_id . "'";

            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $guides[] = $row['name'];
            }
        }
        return (!empty($guides[0])) ? $guides[0] : "";
    }
}
