<?php

namespace ODE\Service;

use DBManagerFactory;

/**
 * Class DocumentService
 *
 * @package ODE\Service
 */
class DocumentService
{
    /**
     * Fonction qui retourne l'object document
     * @param string   $document_id
     * @return false|object
     */
    static function get($document_id = '')
    {
        $db = DBManagerFactory::getInstance();
        if (!empty($document_id)) {
            $sql = "SELECT * FROM `documents` WHERE `id` = '" . $document_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $document = $row;
            }
            if (is_array($document) && count($document) > 0) {
                $document['module_type'] = 'Document';
                $document['name_display'] = $document['name'];
            }
        } else {
            $GLOBALS['log']->fatal("DocumentService::get() => L'id de l'document à récupérer est vide");
        }
        return (is_array($document) && count($document) > 0) ? (object) $document : false;
    }
}
