<?php

namespace ODE\Service;

use DBManagerFactory;

/**
 * Class LiquidationService
 *
 * @package ODE\Service
 */
class LiquidationService
{
    /**
     * Fonction qui retourne l'object liquidation
     * @param string   $liquidation_id
     * @return false|object
     */
    static function get($liquidation_id = '')
    {
        $db = DBManagerFactory::getInstance();
        if (!empty($liquidation_id)) {
            $sql = "SELECT * FROM `ops_liquidation` WHERE `id` = '" . $liquidation_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $liquidation = $row;
            }
            if (is_array($liquidation) && count($liquidation) > 0) {
                $liquidation['module_type'] = 'OPS_liquidation';
                $liquidation['name_display'] = $liquidation['name'];
            }
        } else {
            $GLOBALS['log']->fatal("LiquidationService::get() => L'id de l'liquidation à récupérer est vide");
        }
        return (is_array($liquidation) && count($liquidation) > 0) ? (object) $liquidation : false;
    }
}
