<?php

namespace ODE\Service;

use BeanFactory;

use DBManagerFactory;

/**
 * Class JustificatifService
 *
 * @package ODE\Service
 */
class JustificatifService
{

    /**
     * Fonction qui retourne l'object justificatif
     * @param string   $justificatif_id
     * @return false|object
     */
    static function get($justificatif_id = '')
    {

        global $sugar_config;
        $db = DBManagerFactory::getInstance();

        if (!empty($justificatif_id)) {
            $sql = "SELECT * FROM `ops_justificatif` WHERE `id` = '" . $justificatif_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $justificatif = $row;
            }
            if (is_array($justificatif) && count($justificatif) > 0) {
                $justificatif['module_type'] = 'OPS_justificatif';
                $justificatif['name_display'] = $justificatif['name'];
                $type = JustificatifService::getType($justificatif['id']);
                $justificatif['ops_type_document_id'] = $type !== false ? $type->id : '';
                $justificatif['ops_type_document_name'] = $type !== false ? $type->name : '';
                $obj_justificatif = BeanFactory::getBean('OPS_justificatif', $justificatif['id']);
                if ($obj_justificatif->ops_personne_morale_id) {
                    $chemin = $sugar_config['ops_justificatif']['chemin'] . '/' . $obj_justificatif->ops_personne_morale_id . '/' . $justificatif['id'];
                } else {
                    $chemin = $sugar_config['ops_justificatif']['chemin'] . '/' . $obj_justificatif->ops_dossier_id . '/' . $justificatif['id'];
                }
                $justificatif['file_path'] = $chemin;
                $justificatif['file_size'] = JustificatifService::getFileSize($chemin);
                $justificatif['file_size_display'] = JustificatifService::getFileSizeDisplay($chemin);
            }
        } else {
            $GLOBALS['log']->fatal("JustificatifService::get() => L'id du justificatif à récupérer est vide");
        }
        return (is_array($justificatif) && count($justificatif) > 0) ? (object) $justificatif : false;
    }

    /**
     * Fonction qui retourne le type_document du justificatif
     * @param string   $justificatif_id
     * @return false|object
     */
    static function getType($justificatif_id = '')
    {

        $db = DBManagerFactory::getInstance();
        if (!empty($justificatif_id)) {
            $sql = "SELECT ops_type_document_id FROM `ops_type_document_ops_justificatif` WHERE `ops_justificatif_id` = '" . $justificatif_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                if (!empty($row['ops_type_document_id'])) {
                    $sql = "SELECT * FROM `ops_type_document` WHERE `id` = '" . $row['ops_type_document_id'] . "' AND `deleted` = '0' ";
                    $result = $db->query($sql);
                    while ($row = $db->fetchByAssoc($result)) {
                        $type_document = $row;
                    }
                    if (is_array($type_document) && count($type_document) > 0) {
                        $type_document['module_type'] = 'OPS_type_document';
                        $type_document['name_display'] = $type_document['name'];
                    }
                }
            }
        } else {
            $GLOBALS['log']->fatal("DossierService::getType() => L'id du dossier est vide");
        }
        return (is_array($type_document) && count($type_document) > 0) ? (object) $type_document : false;
    }

    /**
     * Fonction qui retourne la taille d'un fichier si il existe sinon 0
     * @param string   $file_path
     * @return int
     */
    static function getFileSize($file_path = '')
    {
        return !empty($file_path) && file_exists($file_path) ? filesize($file_path) : 0;
    }

    /**
     * Fonction qui retourne la taille du fichier à afficher
     * @param string   $file_path
     * @return string
     */
    static function getFileSizeDisplay($file_path = '')
    {
        $file_size = JustificatifService::getFileSize($file_path);
        if ($file_size < 1000) return $file_size . ' Octets';
        if ($file_size < 1000000) return round($file_size / 1024, 2) . ' Ko';
        if ($file_size < 1000000000) return round($file_size / (1024 * 1024), 2) . ' Mo';
        return round($file_size / (1024 * 1024 * 1024), 2) . ' Go';
    }
}
