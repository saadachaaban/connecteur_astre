<?php

namespace ODE\Service;

use DBManagerFactory;

/**
 * Class DispositifService
 *
 * @package ODE\Service
 */
class DispositifService
{

    /**
     * Fonction qui retourne l'object dispositif
     * @param string   $dispositif_id
     * @return false|object
     */
    static function get($dispositif_id)
    {
        $db = DBManagerFactory::getInstance();
        $dispositif = false;
        if (!empty($dispositif_id)) {
            $sql = "SELECT * FROM `ops_dispositif` WHERE `id` = '" . $dispositif_id . "' AND `deleted` = '0' ";
            $query = $db->query($sql);
            $dispositif = $db->fetchByAssoc($query);
        } else {
            $GLOBALS['log']->fatal("DispositifService::get() => L'id est vide ");
        }
        return (is_array($dispositif) && count($dispositif) > 0) ? (object) $dispositif : false;
    }

    /**
     * Fonction qui retourne la liste des dispositif de la plateforme
     * @return array
     */
    static function getListe()
    {
        $db = DBManagerFactory::getInstance();
        $dispositifs = [];
        $sql = "SELECT * FROM `ops_dispositif` WHERE `deleted` = '0' ";
        $query = $db->query($sql);

        while ($dispositif = $db->fetchByAssoc($query)) {
            $dispositifs[] = (object) $dispositif;
        }
        return (is_array($dispositifs) && count($dispositifs) > 0) ? $dispositifs : [];
    }

    /**
     * Fonction qui retourne le guides d'instruction du dispositif
     * @param string   $dispositif_id
     * @return false|object
     */
    static function getGuideInstruction($dispositif_id)
    {
        $db = DBManagerFactory::getInstance();

        if (!empty($dispositif_id)) {

            $sql = "SELECT * FROM `ops_guide_instruction`
                    WHERE `id` IN (
                        SELECT `ops_guide_instruction_id` FROM `ops_guide_instruction_ops_dispositif` 
                        WHERE `ops_dispositif_id` = '" . $dispositif_id . "' AND `deleted` = '0' 
                    )";

            $query = $db->query($sql);

            while ($guide_query = $db->fetchByAssoc($query)) {

                $guide = [
                    'id' => $guide_query['id'],
                    'name' => $guide_query['name'],
                    'etapes' => DispositifService::getEtapes($guide_query['id'])
                ];
            }
        } else {
            $GLOBALS['log']->fatal("DispositifService::getGuideInstruction() => 'dispositif_id' est vide ");
        }

        return (is_array($guide) && count($guide) > 0) ? json_decode(json_encode($guide)) : false;
    }

    /**
     * Fonction qui retourne les étapes du guide d'instruction
     * @param string   $guide_instruction_id
     * @return false|object
     */
    static function getEtapes($guide_instruction_id)
    {

        $db = DBManagerFactory::getInstance();
        $etapes = [];

        if (!empty($guide_instruction_id)) {

            $sql = "SELECT * FROM `ops_etape`
                        WHERE `id` IN (
                            SELECT `ops_etape_id` FROM `ops_guide_instruction_ops_etape` 
                            WHERE `ops_guide_instruction_id` = '" . $guide_instruction_id . "' AND `deleted` = '0' 
                        )";

            $query = $db->query($sql);

            while ($etape_query = $db->fetchByAssoc($query)) {

                $etape = [
                    'id' => $etape_query['id'],
                    'name' => $etape_query['name'],
                    'statuts' => DispositifService::getStatuts($etape_query['id'])
                ];

                $etapes[] = $etape;
            }
        } else {
            $GLOBALS['log']->fatal("DispositifService::getEtapes() => 'guide_instruction_id' est vide ");
        }

        return (is_array($etapes) && count($etapes) > 0) ? json_decode(json_encode($etapes)) : false;
    }

    /**
     * Fonction qui retourne les statuts de l'etape
     * @param string   $etape_id
     * @return false|object
     */
    static function getStatuts($etape_id)
    {

        $db = DBManagerFactory::getInstance();
        $statuts = [];
        if (!empty($etape_id)) {

            $sql = "SELECT * FROM `ops_statut`
                        WHERE `id` IN (
                            SELECT `ops_statut_id` FROM `ops_etape_ops_statut` 
                            WHERE `ops_etape_id` = '" . $etape_id . "' AND `deleted` = '0' 
                        )";

            $query = $db->query($sql);

            while ($statut_query = $db->fetchByAssoc($query)) {
                $statut = [
                    'id' => $statut_query['id'],
                    'name' => $statut_query['name']
                ];
                $statuts[] = $statut;
            }
        } else {
            $GLOBALS['log']->fatal("DispositifService::getStatuts() => 'etape_id' est vide ");
        }

        return (is_array($statuts) && count($statuts) > 0) ? json_decode(json_encode($statuts)) : false;
    }
}
