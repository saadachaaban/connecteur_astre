<?php

namespace ODE\Service;

use DBManagerFactory;

/**
 * Class JournalService
 *
 * @package ODE\Service
 */
class JournalService
{

    /**
     * Fonction qui retourne l'object journal
     * @param string   $statut_id
     * @return false|object
     */
    static function get($journal_id)
    {
        $db = DBManagerFactory::getInstance();
        $journal = false;
        if (!empty($journal_id)) {
            $sql = "SELECT * FROM `ops_journal` WHERE `id` = '" . $journal_id . "' AND `deleted` = '0' ";
            $query = $db->query($sql);
            $journal = $db->fetchByAssoc($query);
            if (!empty($journal['id'])) {
                $journal['details'] = JournalService::getDetails($journal['id']);
            }
        } else {
            $GLOBALS['log']->fatal("JournalService::get() => L'id  est vide ");
        }
        return (is_array($journal) && count($journal) > 0) ? (object) $journal : false;
    }

    /**
     * Fonction qui retourne l'object journal par parent_type et parent_id
     * @param string   $parent_type
     * @param string   $parent_id
     * @return false|object
     */
    static function getByParent($parent_type, $parent_id)
    {
        $db = DBManagerFactory::getInstance();
        $journal = false;
        if (!empty($parent_type) && !empty($parent_id)) {
            $sql = "SELECT * FROM `ops_journal` WHERE `parent_id` = '" . $parent_id . "' AND `parent_type` = '" . $parent_type . "' AND `deleted` = '0' ";
            $query = $db->query($sql);
            $journal = $db->fetchByAssoc($query);
            if (!empty($journal['id'])) {
                $journal['details'] = JournalService::getDetails($journal['id']);
            }
        } else {
            $GLOBALS['log']->fatal("JournalService::getAppairage() => L'id ou le type de parent est vide ( type='" . $parent_type . "' , id='" . $parent_id . "' )");
        }
        return (is_array($journal) && count($journal) > 0) ? (object) $journal : false;
    }

    /**
     * Fonction qui retourne l'object statut
     * @param string   $journal_id
     * @return array
     */
    static function getDetails($journal_id)
    {
        $db = DBManagerFactory::getInstance();
        $details = [];
        if (!empty($journal_id)) {
            $sql = "SELECT * FROM ops_journal_detail AS detail
            INNER JOIN ops_journal_detail_ops_journal AS relation ON detail.id=relation.ops_journal_detail_id 
            AND relation.deleted='0' AND relation.ops_journal_id = '" . $journal_id . "' WHERE detail.deleted='0'";
            $result = $db->query($sql);
            while ($detail = $db->fetchByAssoc($result)) {
                if (is_array($detail) && count($detail) > 0) {
                    $detail['module_type'] = 'OPS_journal_detail';
                    $details[] = (object) $detail;
                }
            }
        } else {
            $GLOBALS['log']->fatal("JournalService::get() => L'id de l'journal à récupérer est vide");
        }
        return (is_array($details) && count($details) > 0) ? (object) $details : [];
    }

    /**
     * Fonction qui supprime définitivement les détails d'un journal
     * @param string   $journal_id
     * @return void
     */
    static function deleteDetails($journal_id)
    {
        $db = DBManagerFactory::getInstance();
        if (!empty($journal_id)) {
            $details_sql = "DELETE FROM ops_journal_detail WHERE id IN ( SELECT detail.id FROM ops_journal_detail AS detail
            INNER JOIN ops_journal_detail_ops_journal AS relation ON detail.id=relation.ops_journal_detail_id 
            AND relation.ops_journal_id = '" . $journal_id . "')";
            $details_result = $db->query($details_sql);
            if (!$details_result) {
                $GLOBALS['log']->fatal("JournalService::deleteDetails() => Echec de la suppression des relations 'ops_journal_detail_ops_journal' du journal id =" . $journal_id . " Erreur DB :" . $db->lastError());
            }
            $relations_sql = "DELETE FROM ops_journal_detail_ops_journal WHERE ops_journal_id = '" . $journal_id . "'";
            $relation_result = $db->query($relations_sql);
            if (!$relation_result) {
                $GLOBALS['log']->fatal("JournalService::deleteDetails() => Echec de la suppression des relations 'ops_journal_detail_ops_journal' du journal id =" . $journal_id . " Erreur DB :" . $db->lastError());
            }
        } else {
            $GLOBALS['log']->fatal("JournalService::deleteDetails() => L'id de journal à est vide");
        }
    }
}
