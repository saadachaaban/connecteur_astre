<?php

namespace ODE\Service;

use ODE\Service\LiquidationService;

use DBManagerFactory;

/**
 * Class EngagementService
 *
 * @package ODE\Service
 */
class EngagementService
{

    /**
     * Fonction qui retourne l'object engagement
     * @param string   $engagement_id
     * @return false|object
     */
    static function get($engagement_id = '')
    {
        $db = DBManagerFactory::getInstance();
        if (!empty($engagement_id)) {
            $sql = "SELECT * FROM `ops_engagement` WHERE `id` = '" . $engagement_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $engagement = $row;
            }
            if (is_array($engagement) && count($engagement) > 0) {
                $engagement['module_type'] = 'OPS_engagement';
                $engagement['name_display'] = $engagement['name'];
            }
        } else {
            $GLOBALS['log']->fatal("EngagementService::get() => L'id de l'engagement à récupérer est vide");
        }
        return (is_array($engagement) && count($engagement) > 0) ? (object) $engagement : false;
    }

    /**
     * Fonction qui retourne les liquidations de l'engagement
     * @param string   $engagement_id
     * @return array
     */
    static function getLiquidations($engagement_id = '')
    {
        $db = DBManagerFactory::getInstance();
        $liquidations = [];
        if (!empty($engagement_id)) {
            $sql = "SELECT ops_liquidation_id FROM `ops_engagement_ops_liquidation` WHERE `ops_engagement_id` = '" . $engagement_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $liquidation = LiquidationService::get($row['ops_liquidation_id']);
                if ($liquidation !== false) {
                    $liquidations[] = $liquidation;
                }
            }
        } else {
            $GLOBALS['log']->fatal("EngagementService::getLiquidations() => L'id du engagement est vide");
        }
        return (is_array($liquidations) && count($liquidations) > 0) ? $liquidations : [];
    }
}
