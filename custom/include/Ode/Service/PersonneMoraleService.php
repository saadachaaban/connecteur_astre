<?php

namespace ODE\Service;

use ODE\Service\DomiciliationService;

use DBManagerFactory;

/**
 * Class PersonneMoraleService
 *
 * @package ODE\Service
 */
class PersonneMoraleService
{
    /**
     * Fonction qui retourne l'object personne morale
     * @param string   $personne_morale_id
     * @return false|object
     */
    static function get($personne_morale_id)
    {
        $db = DBManagerFactory::getInstance();
        $personne_morale = [];
        if (!empty($personne_morale_id)) {
            $sql = "SELECT * FROM `ops_personne_morale` WHERE `id` = '" . $personne_morale_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $personne_morale = $row;
            }
            if (is_array($personne_morale) && count($personne_morale) > 0) {
                $personne_morale['module_type'] = 'OPS_personne_morale';
                $personne_morale['name_display'] = $personne_morale['name'];
            }
        } else {
            $GLOBALS['log']->fatal("PersonneMoraleService::get() => L'id de la personne morale à récupérer est vide");
        }
        return (is_array($personne_morale) && count($personne_morale) > 0) ? (object) $personne_morale : false;
    }

    /**
     * Fonction qui retourne l'appairage d'une personne morale 
     * @param string   $personne_morale_id
     * @param string   $appairage_type
     * @param string   $appairage_statut
     * @return false|object
     */
    static function getAppairage($personne_morale_id, $appairage_type = '', $appairage_statut = '')
    {
        $db = DBManagerFactory::getInstance();
        $appairage = false;
        if (!empty($personne_morale_id)) {
            $sql = "SELECT * FROM `ops_appairage` WHERE `parent_id` = '" . $personne_morale_id . "' AND `parent_type` = 'OPS_personne_morale' ";
            $sql .= (!empty($appairage_type)) ? " AND `logiciel` = '" . $appairage_type . "' " : '';
            $sql .= (!empty($appairage_statut)) ? " AND `statut` = '" . $appairage_statut . "' " : '';
            $sql .= " AND `deleted` = '0' ";
            $query = $db->query($sql);
            $appairage = $db->fetchByAssoc($query);
        } else {
            $GLOBALS['log']->fatal("PersonneMoraleService::getAppairage() => L'id de la personne morale est vide");
        }

        return (is_array($appairage) && count($appairage) > 0) ? (object) $appairage : false;
    }

    /**
     * Fonction qui retourne des domiciliations de la personne morale
     * @param string   $personne_morale_id
     * @return array
     */
    static function getDomiciliations($personne_morale_id = '')
    {
        $db = DBManagerFactory::getInstance();
        $domiciliations = [];
        if (!empty($personne_morale_id)) {
            $sql = "SELECT ops_domiciliation_id FROM `ops_personne_morale_ops_domiciliation` WHERE `ops_personne_morale_id` = '" . $personne_morale_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $domiciliation = DomiciliationService::get($row['ops_domiciliation_id']);
                if ($domiciliation !== false) {
                    $domiciliations[] = $domiciliation;
                }
            }
        } else {
            $GLOBALS['log']->fatal("PersonneMoraleService::getDomiciliations() => L'id de l'personne_morale est vide");
        }
        return (is_array($domiciliations) && count($domiciliations) > 0) ? $domiciliations : [];
    }
}
