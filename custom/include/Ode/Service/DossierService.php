<?php

namespace ODE\Service;

use ODE\Helper\OdeArrayHelper;
use ODE\Service\IndividuService;
use ODE\Service\PersonneMoraleService;
use ODE\Service\EngagementService;
use ODE\Service\DocumentService;
use ODE\Service\JustificatifService;

use DBManagerFactory;

/**
 * Class DossierService
 *
 * @package ODE\Service
 */
class DossierService
{

    /**
     * Fonction qui retourne l'object dossier
     * @param string   $dossier_id
     * @return false|object
     */
    static function get($dossier_id)
    {
        $db = DBManagerFactory::getInstance();
        $dossier = [];
        if (!empty($dossier_id)) {
            $sql = "SELECT * FROM `ops_dossier` WHERE `id` = '" . $dossier_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $dossier = $row;
            }
            if (is_array($dossier) && count($dossier) > 0) {
                $dossier['module_type'] = 'OPS_dossier';
            }
            if (!empty($dossier['champs_custom'])) {
                $champs_custom_json_retour = OdeArrayHelper::jsonToArray(base64_decode($dossier['champs_custom']));
                if ($champs_custom_json_retour['statut'] === "ok") {
                    $champs_custom = $champs_custom_json_retour['data'];
                    if (is_array($champs_custom) && count($champs_custom) > 0) {
                        $dossier = array_merge($dossier, $champs_custom);
                        unset($dossier['champs_custom']);
                    }
                } else {
                    $GLOBALS['log']->fatal("DossierService::get() =>  Les champs custom du dossier id = " . $dossier_id . " n'ont pas été récupérés ( " . $champs_custom_json_retour['data'] . " ) ");
                }
            }
        } else {
            $GLOBALS['log']->fatal("DossierService::get() => L'id du dossier à récupérer est vide");
        }
        return (is_array($dossier) && count($dossier) > 0) ? (object) $dossier : false;
    }

    /**
     * Fonction qui retourne le demandeur du dossier
     * @param string   $dossier_id
     * @param string   $demandeur_type
     * @return false|object
     */
    static function getDemandeur($dossier_id, $demandeur_type = '')
    {
        $db = DBManagerFactory::getInstance();
        $demandeur = false;

        do {

            if (empty($dossier_id)) break;

            if (empty($demandeur_type) || !in_array($demandeur_type, ['Individu', 'Personne Morale'])) {
                $dossier = DossierService::get($dossier_id);
                $demandeur_type = ($dossier->type_tiers) ? $dossier->type_tiers : '';
            }

            switch ($demandeur_type) {
                case 'Personne Morale':
                    $sql_personne_morale_id = "SELECT `ops_personne_morale_id` FROM `ops_personne_morale_ops_dossier` WHERE `ops_dossier_id` = '" . $dossier_id . "' AND `deleted` = '0' ";
                    $query_personne_morale_id = $db->query($sql_personne_morale_id);
                    $result_personne_morale_id = $db->fetchByAssoc($query_personne_morale_id);
                    if (!empty($result_personne_morale_id['ops_personne_morale_id'])) {
                        $demandeur = PersonneMoraleService::get($result_personne_morale_id['ops_personne_morale_id']);
                    }
                    break;
                case 'Individu':
                    $sql_individu_id = "SELECT `ops_individu_id` FROM `ops_individu_ops_dossier` WHERE `ops_dossier_id` = '" . $dossier_id . "' AND `deleted` = '0' ";
                    $query_individu_id = $db->query($sql_individu_id);
                    $result_individu_id = $db->fetchByAssoc($query_individu_id);
                    if (!empty($result_individu_id['ops_individu_id'])) {
                        $demandeur = IndividuService::get($result_individu_id['ops_individu_id']);
                    }
                    break;
            }
        } while (0);

        return $demandeur;
    }

    /**
     * Fonction qui retourne les engagements du dossier
     * @param string   $dossier_id
     * @return array
     */
    static function getEngagements($dossier_id = '')
    {
        $db = DBManagerFactory::getInstance();
        $engagements = [];
        if (!empty($dossier_id)) {
            $sql = "SELECT ops_engagement_id FROM `ops_engagement_ops_dossier` WHERE `ops_dossier_id` = '" . $dossier_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $engagement = EngagementService::get($row['ops_engagement_id']);
                if ($engagement !== false) {
                    $engagements[] = $engagement;
                }
            }
        } else {
            $GLOBALS['log']->fatal("DossierService::getEngagements() => L'id du dossier est vide");
        }
        return (is_array($engagements) && count($engagements) > 0) ? $engagements : [];
    }

    /**
     * Fonction qui retourne les documents du dossier
     * @param string   $dossier_id
     * @return array
     */
    static function getDocuments($dossier_id = '')
    {
        $db = DBManagerFactory::getInstance();
        $documents = [];
        if (!empty($dossier_id)) {
            $sql = "SELECT document_id FROM `ops_dossier_documents` WHERE `ops_dossier_id` = '" . $dossier_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $document = DocumentService::get($row['document_id']);
                if ($document !== false) {
                    $documents[] = $document;
                }
            }
        } else {
            $GLOBALS['log']->fatal("DossierService::getDocuments() => L'id du dossier est vide");
        }
        return (is_array($documents) && count($documents) > 0) ? $documents : [];
    }

    /**
     * Fonction qui retourne les justificatifs du dossier
     * @param string   $dossier_id
     * @return array
     */
    static function getJustificatifs($dossier_id = '')
    {
        $db = DBManagerFactory::getInstance();
        $justificatifs = [];
        if (!empty($dossier_id)) {
            $sql = "SELECT ops_justificatif_id FROM `ops_dossier_ops_justificatif` WHERE `ops_dossier_id` = '" . $dossier_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $justificatif = JustificatifService::get($row['ops_justificatif_id']);
                if ($justificatif !== false) {
                    $justificatifs[] = $justificatif;
                }
            }
        } else {
            $GLOBALS['log']->fatal("DossierService::getJustificatifs() => L'id du dossier est vide");
        }
        return (is_array($justificatifs) && count($justificatifs) > 0) ? $justificatifs : [];
    }
}



/*
if(!$result) {
    $GLOBALS['log']->fatal("delete_responsable_id_SQL : ".$db->lastError());
} 
*/