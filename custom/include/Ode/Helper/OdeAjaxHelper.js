String.prototype.utf8ToB64 = function() {
    return window.btoa(unescape(encodeURIComponent( this )));
};

Object.getLength = function(obj) {
    var size = 0,key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

jQuery.extend({
    stringify  : function stringify(obj) {
        var t = typeof (obj);
        if (t != "object" || obj === null) {
            // simple data type
            if (t == "string") obj = '"' + obj + '"';
            return String(obj);
        } else {
            // recurse array or object
            var n, v, json = [], arr = (obj && obj.constructor == Array);

            for (n in obj) {
                v = obj[n];
                t = typeof(v);
                if (obj.hasOwnProperty(n)) {
                    if (t == "string") v = '"' + v + '"'; else if (t == "object" && v !== null) v = jQuery.stringify(v);
                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }
            }
            return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
        }
    }
});


var OdeAjax = (function ($) {
    
    return {

        getByAction: function(query, loading ,callback, fail = null) {

            const self = this;
            const module     = query.getModule();
            const action     = query.getAction();
            var _data        = query.getPostData();
            var _data_formated = '';

            function stringifyData(data){
                var time = 60
                if(typeof data === 'object'){
                    $.each(data, function( index, value ) {
                        if (typeof value === 'object' && value.name && value.attache && value.size && value.type) {
                            time += 60;
                        }
                    });
                }
                if(time > 60){
                    loading.apply();
                }
                return new Promise(resolve => {
                setTimeout(() => {
                  resolve(jQuery.stringify(data));
                }, time);
              });
            }

            async function ajaxEnvoi(data){
                var _json_data = await stringifyData(data);
                _stream_data =  String(_json_data).utf8ToB64();
                _formated_data = "&json=" + _stream_data;

                if( typeof _formated_data != 'undefined' && _formated_data != '' )
                {
                    
                    $.ajax({
                        type      : "POST",
                        url       : "index.php?module=" + module + "&action=" + action ,
                        data      : _formated_data,
                        beforeSend: function() {
                            loading.apply();
                        }
                     }).done(function(dataReturned) {
                         var result = self.formatReturnedData(dataReturned);
                         callback.apply(result);
                     }).fail(function(x) {
                        console.log("OdeAjax::getByAction => fail", x );
                        if ( fail !== null && typeof fail === "function") {
                            fail.apply();
                        }
                        if ( typeof OdeLoadingHelper !== 'undefined' ) {
                            OdeLoadingHelper.removeSpinner();
                        }
                     }).always(function() {
                        if ( typeof OdeLoadingHelper !== 'undefined' ) {
                            OdeLoadingHelper.removeSpinner();
                        }
                     });
                }
            }

            ajaxEnvoi(_data);
                    
        },
        
        formatReturnedData: function(dataReturned) {
            var data = {
                statut: false,
                result: {}
            };
            
            dataReturned = ( this.isJson(dataReturned) ) ? JSON.parse(dataReturned) : dataReturned;
            if ( typeof dataReturned === "string" ){
                data.result = this.isAjaxErreur() ? "La ressource demandée n'existe pas" : dataReturned ;
            } else if ( typeof dataReturned === "object" && Object.getLength(dataReturned) > 0 ){
                if ( dataReturned["statut"] !== undefined && dataReturned["statut"] !== null && dataReturned["data"] !== undefined && dataReturned["data"] !== null ){
                    data.statut = (  dataReturned["statut"] === "ok" ) ? true : false;
                    data.result = dataReturned["data"];
                } else {
                    data.result = "Le format retour n'est conforme à ce qui est attendu";
                }
            } else {
                data.result = "La ressource demandée n'existe pas";
            }
            return data;
        },

        isAjaxErreur: function() {
            return false;
        },
    
        isJson: function(item) {

            item = typeof item !== "string"
                ? JSON.stringify(item)
                : item;
        
            try {
                item = JSON.parse(item);
            } catch (e) {
                return false;
            }
        
            if (typeof item === "object" && item !== null) {
                return true;
            }

            return false;
        }

    }

})(jQuery);
