<?php

// Informations liquidation
$mod_strings['LBL_LIQUIDATION_DATE'] = 'Date liquidation';
$mod_strings['LBL_MONTANT_TTC'] = 'Montant liquidé';

// Informations mandatement
$mod_strings['LBL_MANDAT_NUM'] = 'N° mandatement';
$mod_strings['LBL_MANDAT_DATE'] = 'Date mandatement';
$mod_strings['LBL_MANDAT_MONTANT'] = 'Montant mandaté';

// Informations paiement
$mod_strings['LBL_PAIEMENT_NUM'] = 'N° de bordereau';
$mod_strings['LBL_PAIEMENT_DATE'] = 'Date paiement';
$mod_strings['LBL_PAIEMENT_MONTANT'] = 'Montant payé';
