<?php

$dictionary["OPS_engagement"]["fields"]["ligne_credit"]  = array(
  'required' => true,
  'name' => 'ligne_credit',
  'vname' => 'LBL_LIGNE_CREDIT',
  'type' => 'varchar',
  'massupdate' => 0,
  'no_default' => false,
  'comments' => '',
  'help' => '',
  'importable' => 'true',
  'duplicate_merge' => 'disabled',
  'duplicate_merge_dom_value' => '0',
  'audited' => true,
  'inline_edit' => true,
  'reportable' => true,
  'unified_search' => false,
  'merge_filter' => 'disabled',
  'len' => '255',
  'size' => '20',
);

$dictionary["OPS_engagement"]["fields"]["montant_ttc"]['required'] = true;

// Date de création sur Astre
$dictionary["OPS_engagement"]["fields"]["date_creation_astre"]  = array(
  'required' => false,
  'name' => 'date_creation_astre',
  'vname' => 'LBL_DATE_CREATION_ASTRE',
  'type' => 'date',
  'massupdate' => 0,
  'no_default' => false,
  'comments' => '',
  'help' => '',
  'importable' => 'true',
  'duplicate_merge' => 'disabled',
  'duplicate_merge_dom_value' => '0',
  'audited' => true,
  'inline_edit' => true,
  'reportable' => true,
  'unified_search' => false,
  'merge_filter' => 'disabled',
  'size' => '20',
  'enable_range_search' => false,
);
