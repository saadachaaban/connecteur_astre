<?php

$manifest = [
    0 => [
        'acceptable_sugar_versions' => [
            0 => '6.5.25'
        ]
    ],
    1 => [
        'acceptable_sugar_flavors' => [
            0 => 'CE',
            1 => 'PRO',
            2 => 'ENT'
        ]
    ],
    'readme' => '',
    'key' => 'ODE',
    'author' => 'LANTEAS',
    'description' => "Connecteur Astre pour l\'Île de la Réunion",
    'icon' => '',
    'is_uninstallable' => true,
    'name' => "Open Socle - Connecteur Astre",
    'published_date' => '2022-04-11 10:00:00',
    'type' => 'module',
    'version' => '2.2DEV10',
    'remove_tables' => 'false'
];

$installdefs = [

    'id' => 'connecteur_astre',

    'administration' => [
        [
            'from' => '<basepath>/custom/modules/Administration/ops_astre_admin.php'
        ]
    ],

    'scheduledefs' => [
        [
            'from' => '<basepath>/custom/modules/Schedulers/update_astre.php',
        ],
    ],

    'copy' => [
        [
            'from' => '<basepath>/custom/Extension/modules/OPS_engagement/Ext/Language',
            'to' => 'custom/Extension/modules/OPS_engagement/Ext/Language',
        ],
        [
            'from' => '<basepath>/custom/Extension/modules/OPS_engagement/Ext/Vardefs',
            'to' => 'custom/Extension/modules/OPS_engagement/Ext/Vardefs',
        ],
        [
            'from' => '<basepath>/custom/Extension/modules/OPS_liquidation/Ext/Language',
            'to' => 'custom/Extension/modules/OPS_liquidation/Ext/Language',
        ],
        [
            'from' => '<basepath>/custom/Extension/modules/OPS_liquidation/Ext/Vardefs',
            'to' => 'custom/Extension/modules/OPS_liquidation/Ext/Vardefs',
        ],
        [
            'from' => '<basepath>/custom/Extension/modules/OPS_dossier/Ext/ActionViewMap/astre_import.php',
            'to' => 'custom/Extension/modules/OPS_dossier/Ext/ActionViewMap/astre_import.php'
        ],
        [
            'from' => '<basepath>/custom/modules/OPS_engagement',
            'to' => 'custom/modules/OPS_engagement',
        ],
        [
            'from' => '<basepath>/custom/modules/OPS_liquidation',
            'to' => 'custom/modules/OPS_liquidation',
        ],
        [
            'from' => '<basepath>/custom/modules/OPS_dossier',
            'to' => 'custom/modules/OPS_dossier',
        ],
        [
            'from' => '<basepath>/custom/modules/Administration',
            'to' => 'custom/modules/Administration',
        ],
        [
            'from' => '<basepath>/custom/modules/OPS_dispositif',
            'to' => 'custom/modules/OPS_dispositif',
        ],
        [
            'from' => '<basepath>/modules/OPS_dispositif/controller.php',
            'to' => 'modules/OPS_dispositif/controller.php',
        ],
        [
            'from' => '<basepath>/custom/include/Ode',
            'to' => 'custom/include/Ode',
        ],
        [
            'from' => '<basepath>/custom/include/Astre',
            'to' => 'custom/include/Astre',
        ]

    ],

    'language' => [
        [
            'from' => '<basepath>/custom/modules/Administration/language/ops_astre.fr_FR.php',
            'to_module' => 'Administration',
            'language' => 'fr_FR'
        ],
        [
            'from' => '<basepath>/custom/modules/Schedulers/language/fr_FR.lang.php',
            'to_module' => 'Schedulers',
            'language' => 'fr_FR'
        ],
        [
            'from' => '<basepath>/custom/modules/Schedulers/language/fr_FR.lang.php',
            'to_module' => 'Schedulers',
            'language' => 'en_us'
        ],
    ],


];
